'use strict'

//Import cross origins
const cors = require('cors');
//Import express
const express = require('express');
//Import Configurations 
const config = require('./config/config');
//import controllers js
const routes = require('./routes');
//import MongoDB
var mongoose = require('mongoose');
//import http
const http = require('http');

const server = express();
//Allow cross origins
server.use(cors());
//Set constant server port
const server_port = config.web_port;
//set routes
server.use(routes);
server.use(express.static(__dirname));

//Database Connection initiation
mongoose.connect('mongodb:' + config.database);
mongoose.set('debug', true);
var database = mongoose.connection;

//import http library
var httpServer = http.createServer(server);

//require socket
const io = require('./services/socketService').listen(httpServer);
// const iop = require('./services/passengerSocketService').listen(httpServer);
//start server...
httpServer.listen(server_port, err => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('server listening on port : ' + server_port);
});







