var data = {
    "paramters": [
        {
            "startTime": "04:00:00",
            "endTime": "06:30:00",
            "fixedInterval": 30,
            "noOfBusses": "1",
            "slots": 5,
            "passengerAverage": []
        },
        {
            "startTime": "06:30:00",
            "endTime": "07:30:00",
            "fixedInterval": 10,
            "noOfBusses": "8",
            "slots": 6,
            "passengerAverage": []
        },
        {
            "startTime": "07:30:00",
            "endTime": "08:30:00",
            "fixedInterval": 15,
            "noOfBusses": "6",
            "slots": 4,
            "passengerAverage": []
        },
        {
            "startTime": "08:30:00",
            "endTime": "10:00:00",
            "fixedInterval": 20,
            "noOfBusses": "6",
            "slots": 5,
            "passengerAverage": []
        },
        {
            "startTime": "10:00:00",
            "endTime": "12:30:00",
            "fixedInterval": 30,
            "noOfBusses": "6",
            "slots": 5,
            "passengerAverage": []
        },
        {
            "startTime": "12:30:00",
            "endTime": "13:30:00",
            "fixedInterval": 15,
            "noOfBusses": "8",
            "slots": 4,
            "passengerAverage": []
        },
        {
            "startTime": "13:30:00",
            "endTime": "14:30:00",
            "fixedInterval": 10,
            "noOfBusses": "14",
            "slots": 6,
            "passengerAverage": []
        },
        {
            "startTime": "14:30:00",
            "endTime": "15:30:00",
            "fixedInterval": 15,
            "noOfBusses": "5",
            "slots": 4,
            "passengerAverage": []
        },
        {
            "startTime": "15:30:00",
            "endTime": "16:30:00",
            "fixedInterval": 30,
            "noOfBusses": "3",
            "slots": 2,
            "passengerAverage": []
        },
        {
            "startTime": "16:30:00",
            "endTime": "18:30:00",
            "fixedInterval": 30,
            "noOfBusses": "6",
            "slots": 4,
            "passengerAverage": []
        },
        {
            "startTime": "18:30:00",
            "endTime": "20:30:00",
            "fixedInterval": 30,
            "noOfBusses": "8",
            "slots": 4,
            "passengerAverage": []
        },
        {
            "startTime": "20:30:00",
            "endTime": "23:00:00",
            "fixedInterval": 30,
            "noOfBusses": "8",
            "slots": 5,
            "passengerAverage": []
        }
    ],
    "numberOfIterations": 100,
    "routeNo": "5ba79bf9b62292163497ac6e"
}

module.exports.timeAllocations = data;