'use strict'
//Import Express
const express = require('express');
//user router
const router = express.Router();
//Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

//import  controllers
const userController = require('../src/user/usersController');
const locationDataController = require('../src/driver-model/locationData/locationDataController');
const journeyController = require('../src/driver-model/journeyData/journeyController');
const trackingController = require('../src/driver-model/busTracking/trackingController');
const busRouteController = require('../src/driver-model/busRoute/busRouteController');
const busController = require('../src/bus/busController');
const scheduleController = require('../src/bus-allocation/scheduleController');
const predictionController = require('../src/driver-model/passengerPrediction/passengerPredictionController');
const passengerController = require('../src/passenger/passengerController');
const passengerTrackingController = require('../src/passenger-model/passengerTracking/passengerTrackingController');
const passengerLocationDataController = require('../src/passenger-model/passengerLocationData/passengerLocationDataController');
const nearestBusstopController = require('../src/passenger-model/NearestBusstop/NearestBusstopController');
const nearestBusController = require('../src/passenger-model/nearestBus/nearestBusController');
const passengerMovementController = require('../src/passenger-model/passengerMovement/passengerMovementController');
const behaviorAnalysisController = require('../src/passenger-model/behaviorAnalysis/behaviorAnalysisController');
const etaPredictionController = require('../src/passenger-model/etaPrediction/etaPredictionController');
const emergencyController = require('../src/driver-model/emergency-alerts/emergencyController');

//import Validator class
const validator = require('../validators/validator');

//import validator Schemas 
const userSchema = require('../src/user/userSchema');
const busRouteSchema = require('../src/driver-model/busRoute/busRouteSchema');
const busSchema = require('../src/bus/busSchema');
const trackerSchema = require('../src/driver-model/busTracking/trackingSchema');
const scheduleSchema = require('../src/bus-allocation/scheduleSchema');
const predictionSchema = require('../src/driver-model/passengerPrediction/passengerPredictionSchema');
const passengerSchema = require('../src/passenger/passengerSchema');
const passengerTrackingSchema = require('../src/passenger-model/passengerTracking/passengerTrackingSchema');
const emergencySchema = require('../src/driver-model/emergency-alerts/emergencySchama');

//user routes
router.route('/api/user/new').post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/api/user/login').post(validator.validateBody(userSchema.login), userController.login);
router.route('/api/user/changePassword').put(validator.validateBodyWithToken(userSchema.changePassword), userController.changePassword);
router.route('/api/user/remove').post(validator.validateBodyWithToken(userSchema.user_id), userController.RemoveUser);
router.route('/api/user/users').get(validator.validateHeader(), userController.getUsers);
router.route('/api/user/getUser').post(validator.validateBodyWithToken(userSchema.user_id), userController.getUserById);
router.route('/api/user/update').post(validator.validateBodyWithToken(userSchema.updateUser), userController.updateUser);

//passenger routes
router.route('/api/passenger/signup').post(validator.validateBody(passengerSchema.signUp), passengerController.signUp);
router.route('/api/passenger/signin').post(validator.validateBody(passengerSchema.signIn), passengerController.signIn);
router.route('/api/passenger/changePassword').post(validator.validateBody(passengerSchema.changePassword), passengerController.changePassword);
router.route('/api/passenger/updatePassenger').post(validator.validateBody(passengerSchema.updatePassenger), passengerController.updatePassenger);

//locationData routes
router.route('/api/cordinate/new').post(locationDataController.newCordinate);
router.route('/api/cordinate/getRawData').post(locationDataController.getRawCordinates);

//tracking routes
router.route('/api/tracking/new').post(validator.validateBodyWithToken(trackerSchema.newTracker), trackingController.newTracking);
router.route('/api/tracking/update').put(validator.validateBodyWithToken(trackerSchema.updateTracker), trackingController.updateTracker);
router.route('/api/tracking/get').get(validator.validateHeader(), trackingController.getTrackers);
router.route('/api/tracking/route').post(validator.validateBodyWithToken(trackerSchema.route), trackingController.getRouteActiveTrackers);
router.route('/api/tracking/remove').post(validator.validateBodyWithToken(trackerSchema.trackerID), trackingController.removeTracker);
router.route('/api/tracking/byLocation').post(validator.validateBodyWithToken(trackerSchema.route), trackingController.getActiveTrackersByLocation);


//journey routes
router.route('/api/journey/create').post(journeyController.startJourney);
router.route('/api/journey/end').post(journeyController.journeyEnd);
router.route('/api/journey/get').post(journeyController.journeyEnd);

//busRoute route
router.route('/api/route/new').post(validator.validateBodyWithToken(busRouteSchema.newRoute), busRouteController.newBusRoute);
router.route('/api/route/edit').post(validator.validateBodyWithToken(busRouteSchema.editRoute), busRouteController.updateRoute);
router.route('/api/route/findByID').post(validator.validateBodyWithToken(busRouteSchema.route_id), busRouteController.findByID);
router.route('/api/route/findByRouteNo').post(validator.validateBodyWithToken(busRouteSchema.route_no), busRouteController.findByRouteNo);
router.route('/api/route/remove').post(validator.validateBodyWithToken(busRouteSchema.route_id), busRouteController.removeRoute);
router.route('/api/route/getAll').get(validator.validateHeader(), busRouteController.getAllRoutes);

//bus route
router.route('/api/bus/new').post(validator.validateBodyWithToken(busSchema.newBus), busController.newBus);
router.route('/api/bus/update').put(validator.validateBodyWithToken(busSchema.updateBus), busController.updateBus);
router.route('/api/bus/get').get(busController.getBuses);
router.route('/api/bus/findByBusNo').post(validator.validateBodyWithToken(busSchema.busNo), busController.findFromBusNo);
router.route('/api/bus/findByBusID').post(validator.validateBodyWithToken(busSchema.busID), busController.findFromID);
router.route('/api/bus/remove').delete(validator.validateBodyWithToken(busSchema.busID), busController.removeBus);
router.route('/api/bus/activeBusesFromRoute').post(validator.validateBodyWithToken(busSchema.activeBus), busController.getActiveBusesFromRoute);
router.route('/api/bus/activeBusesByStartingLocation').post(validator.validateBodyWithToken(busSchema.activeBusByLocation), busController.getActiveBusesByLocation);



//schedules
router.route('/api/schedule/generateSchedule').post(validator.validateBodyWithToken(scheduleSchema.createSchedule), scheduleController.createSchedule);
router.route('/api/passenger/prediction').get(predictionController.getPrediction);
router.route('/api/passenger/getPredictedData').post(validator.validateBodyWithToken(predictionSchema.getPredictedData), predictionController.getPredictedData);
router.route('/api/schedule/get').post(scheduleController.getSchedule);

//passengerTracking
router.route('/api/passengerTracking/startTracker').post(validator.validateBodyWithToken(passengerTrackingSchema.newPassengerTracker), passengerTrackingController.newPassengerTracking);
router.route('/api/passengerTracking/updateTracker').put(validator.validateBodyWithToken(passengerTrackingSchema.updatePassengerTracker), passengerTrackingController.updatePassengerTracker);
router.route('/api/passengerTracking/removeTracker').post(validator.validateBodyWithToken(passengerTrackingSchema.trackerID), passengerTrackingController.removePassengerTracker);

//passengerCoordinates
router.route('/api/passengerLocationData/savePassengerCoordinates').post(passengerLocationDataController.saveNewPassengerCoordinate);
router.route('/api/passengerLocationData/getPassengercoordinates').post(passengerLocationDataController.getNewRawCoordinates);

//nearest Busstop
router.route('/api/nearestBusstop/getNearestBusstop').post(nearestBusstopController.NearestBusstop);

//nearest Bus
router.route('/api/nearestBus/getNearestBus').post(nearestBusController.nearestBus);

//passengr movement
router.route('/api/passengerMovement/savePassengerMovement').post(passengerMovementController.saveNewPassengerMovement);

//passenger behaviors
router.route('/api/behaviorAnalysis/saveSearchRecord').post(behaviorAnalysisController.updateSearchRecord);
// router.route('/api/behaviorAnalysis/updateSearchRecord').post(behaviorAnalysisController.updateSearchRecord);
router.route('/api/behaviorAnalysis/newBehavior').post(behaviorAnalysisController.newBehavior);
router.route('/api/behaviorAnalysis/getAll').get(behaviorAnalysisController.getAll);

//etaPrediction
router.route('/api/etaPrediction/getEta').post(etaPredictionController.getEta);

//etaPrediction
router.route('/api/emergency/new').post(validator.validateBodyWithToken(emergencySchema.newEmergency), emergencyController.newEmergency);

module.exports = router;