'use strict'
//Imports
const scheduleModelMethods = require('./scheduleModel').exportMethods;
const scheduleModel = require('./scheduleModel').scheduleModel;
const callBackResponse = require('../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactivate";
var exportModel = {};
const itaratedLocalSearch = require('./ils/ILS')
const geneticAlgorithm = require('./ga/genetic_algorithm')
const predictionService = require('../driver-model/passengerPrediction/passengerPredictionService');

/**
 * get scehdule by route from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.getSchedule = (req, res, callBack) => {
    scheduleModelMethods.find({ status: { $ne: deactiveStatus }, date: { $eq: new Date(req.body.date) }, route: req.body.route }, function (err, scheduleObj) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (scheduleObj == null || scheduleObj == undefined || scheduleObj.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No schedules found"));
        } else {
            callBack(callBackResponse.callbackWithData(scheduleObj));
        }
    });
}


/**
 * get scehdule by route from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.createSchedule = (body, res, callBack) => {
    let config = body;
    exportModel.findSchedule(body, res, function (data) {
        if (data.status && body.recreate == false) {
            callBack(callBackResponse.callbackWithfalseMessage("Schedule Already Exists"));
        } else if ((data.status && body.recreate == true) || !data.status) {
            exportModel.deleteSchedule(data.data[0]._id, res, function (data) {
                if (data.status) {
                    predictionService.getPrediction('', '', function (data) {
                        if (data.status) {
                            if (config.type === "GA") {
                                geneticAlgorithm.geneticAlgorithm(config, data.data, function (data) {
                                    data.date = config.date
                                    callBack(callBackResponse.callbackWithData(data));
                                })
                            } else if (config.type === "ILS") {
                                itaratedLocalSearch.generateDailyAllocation(config, data.data, function (data) {
                                    data.date = config.date
                                    exportModel.saveSchedule(data, "", function (data) {
                                        if (data.status) {
                                            callBack(callBackResponse.callbackWithSucessMessage("Schedule Created"));
                                        } else {
                                            callBack(callBackResponse.callbackWithfalseMessage(data.data));
                                        }
                                    })
                                })
                            }

                        } else {
                            callBack(callBackResponse.callbackWithfalseMessage(data.data));
                        }
                    })
                } else {
                    callBack(callBackResponse.callbackWithfalseMessage(data.data));
                }
            })
        } else {
            callBack(callBackResponse.callbackWithfalseMessage(data.data));
        }
    })
}

/**
 * get schedules from today date
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.findSchedule = (body, res, callBack) => {
    scheduleModelMethods.find({ date: body.date }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {

            callBack(callBackResponse.callbackWithfalseMessage("No schedules found"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    })
}

/**
 * remove schedule from the database
 * @param {*} schedule_id 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.deleteSchedule = (schedule_id, res, callBack) => {
    scheduleModelMethods.remove({ _id: schedule_id }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage("No schedules found"));
        } else {
            callBack(callBackResponse.callbackWithData("Schedule Removed"));
        }
    })
}

/**
 * save schedule to database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.saveSchedule = (req, res, callBack) => {
    const newSchedule = new scheduleModel({
        allocation: req.allocation,
        route: req.route,
        busNumbers: req.busNumbers,
        status: activeStatus,
        date: req.date
    });
    scheduleModelMethods.saveSchedule(newSchedule, (err, schedule) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(schedule));
        }
    })
}

module.exports = exportModel;