//import validator class
const joi = require('joi');

//new bus Schema and validations to be done 
module.exports.createSchedule = joi.object().keys({
    recreate: joi.boolean().required(),
    iterationCount: joi.number(),
    route_no: joi.string().required(),
    date: joi.required(),
    type: joi.string().required(),
    generation: joi.number(),
    population: joi.number(),
    crossover: joi.number(),
    mutation: joi.number(),
});
