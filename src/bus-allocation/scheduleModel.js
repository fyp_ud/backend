'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

const scheduleSchema = new Schema({
    date: {
        type: Date,
        default: Date
    },
    allocation: [{
        time_slot: {
            type: String,
            require: true
        },
        allocation: {
            type: Array,
            require: true
        },
        busNumbers: {
            type: Array,
            require: true
        },
        fixedInterval: {
            type: String,
            require: true
        },
        numberOfBusses: {
            type: String,
            require: true
        },
        passengerAverage: {
            type: String,
            require: true
        },
        message: {
            type: String
        }
    }],
    route: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
});

var scheduleModel = mongoose.model("Schedule", scheduleSchema);

exportMethods.saveSchedule = (newSchedule, callback) => {
    newSchedule.save(callback);
}

exportMethods.findOne = (query, callback) => {
    scheduleModel.findOne(query, callback);
}

exportMethods.find = (query, callback) => {
    scheduleModel.find(query, callback);
};

exportMethods.findByID = (query, callback) => {
    scheduleModel.findById(query, callback);
};

exportMethods.remove = (query, callback) => {
    scheduleModel.remove(query, callback);
};

exportMethods.fincByIdandUpdate = (query, scheduleInfo, options, callback) => {
    scheduleModel.findByIdAndUpdate(query, scheduleInfo, options, callback);
}

module.exports = {
    exportMethods,
    scheduleModel
}