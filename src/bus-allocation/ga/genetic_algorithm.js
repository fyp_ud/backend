const busService = require('../../bus/busService')
var returnJson = [];
var POPULATION_SIZE = 15;
var MUT_PROB = 0.01;
var CROSS_PROB = 0.6;
var NO_OF_SLOTS = 5;
var NO_OF_BUSSES = 8;
var MAX_GEN = 250;
var passengerCount = [73, 68, 47, 40, 48];
var id = 0;
var startTime = new Date("October 13, 2014 08:30:00")
var endTime = new Date("October 13, 2014 10:00:00")

var date = new Date()

function getHourDifference(dt1, dt2) {
    let diff = dt2 - dt1;
    return Math.floor((diff / 1000) / 60);
}

var chromosome = function (noOfSlots, noOfBusses) {

    let solution = [];
    let maxValue = (noOfBusses - noOfSlots) + 1;
    let minValue = 1;
    let currentSum = 0;
    let randomValue = 0;
    let isValidRandom;


    for (let i = noOfSlots; i > 0; i--) {


        let sumOfCurrentSolution = solution.reduce((a, b) => a + b, 0);
        let maxRandomForCurrrentIteration = noOfBusses - sumOfCurrentSolution - i + 1;

        if (i == 1) {
            randomValue = maxRandomForCurrrentIteration;
        } else {

            do {
                randomValue = Math.floor((Math.random() * maxValue) + minValue);
                isValidRandom = (maxRandomForCurrrentIteration >= randomValue);

            } while (!isValidRandom);

        }

        do {
            isValidRandom = validateRandomValue((currentSum + randomValue), noOfBusses, );

        }
        while (!isValidRandom);
        currentSum = currentSum + randomValue;

        solution.push(randomValue);

    }

    return solution;

}

function validateRandomValue(currentSum, noOfBusses) {
    return currentSum <= noOfBusses;
}

var initialPopulation = function (size, slots, noOfBuses, passengerAverage, busArray) {

    var generationJson = [];
    for (let i = 0; i < size; ++i) {
        let solution;

        solution = chromosome(slots, noOfBuses);

        //[ 2, 2, 1, 1, 2 ]
        let returnEvaluvateSolution = calcCost(solution, passengerAverage, busArray);

        generationJson.push(returnEvaluvateSolution);

    }

    return generationJson;

}

var calcCost = function (solution, passengerAverage, busArray) {
    let spliceIndex = 0;
    let isEnoughSeats = false;

    let returnEvaluvateJSON = {
        id: 0,
        busAllocation: [],
        busIDs: [],
        fitnessValue: 0
    };

    for (let i = 0; i < solution.length; i++) {
        let sumOfSeats = 0;
        let spliceArray = JSON.parse(JSON.stringify(busArray));
        let newBusArray = spliceArray.splice(spliceIndex, solution[i]);
        spliceIndex = spliceIndex + solution[i];
        newBusArray = JSON.parse(JSON.stringify(newBusArray));
        for (let r = 0; r < newBusArray.length; r++) {
            sumOfSeats = sumOfSeats + newBusArray[r].noOfSeats;
            if (r == 0) {
                returnEvaluvateJSON.busIDs[i] = newBusArray[r].busNo;
            } else {
                returnEvaluvateJSON.busIDs[i] += "," + newBusArray[r].busNo;
            }

        }

        let slack = sumOfSeats - passengerAverage[i];
        if (slack >= 0) {
            isEnoughSeats = true;
        } else {
            isEnoughSeats = false;
        }
        if (slack < -10) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.08;
        } else if (slack < 0) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.2;
        } else if (slack > 100) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.3;
        } else if (slack > 90) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.5;
        } else if (slack > 80) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.7;
        } else if (slack > 70) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 1;
        } else if (slack > 60) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 2;
        } else if (slack > 50) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 4;
        } else if (slack > 40) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 6;
        } else if (slack > 30) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 8;
        } else if (slack > 20) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 10;
        } else if (slack > 5) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 12;
        } else if (slack > 0) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 15;
        }

        //Evaluvating whether the busses are divided in a proper way if the number of busses are more than the actual need
        for (let k = 0; k < passengerAverage.length - 1; k++) {
            for (let j = 0; j < passengerAverage.length; j++) {
                if (passengerAverage[k] > passengerAverage[j]) {
                    if (solution[k] < solution[j]) {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.05;
                    } else {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.15;
                    }
                } else {
                    if (solution[k] > solution[j]) {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.05;
                    } else {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.15;
                    }
                }

            }
        }
    }

    id++;
    returnEvaluvateJSON.id = id;
    returnEvaluvateJSON.busAllocation = solution;
    return returnEvaluvateJSON;
}



// console.log("********** :"+chromosome(NO_OF_SLOTS,NO_OF_BUSSES));




var getFittestParent = function (population) {

    let result = JSON.parse(JSON.stringify(population));
    // result = result.allocation;

    // console.log("++++++++++++++++ population +++++++++++++++++++++++++++++++++");
    // let minResult=result[0];
    // console.log(result);
    let max = result[0].fitnessValue;
    let fittestParent = result[0];
    for (let i = 0; i < result.length; i++) {



        if (result[i].fitnessValue > max) {

            max = result[i].fitnessValue;
            fittestParent = result[i];
        }

    }

    return fittestParent;
}

var getSecondFittestParent = function (population, fittestParent) {

    var result = JSON.parse(JSON.stringify(population));
    if (result) {


        var max = result[0].fitnessValue;
        let secondFittestParent = result[0];
        for (var i = 0; i < result.length; i++) {

            if (fittestParent.id != result[i].id) {

                if (result[i].fitnessValue > max) {

                    max = result[i].fitnessValue;
                    secondFittestParent = result[i];
                }
            }

        }

        return secondFittestParent;
    }
};

var getLeastFittestParent = function (population) {

    let result = JSON.parse(JSON.stringify(population));
    let min = result[0].fitnessValue;
    let leastfittestParent = result[0];
    for (let i = 0; i < result.length; i++) {



        if (result[i].fitnessValue < min) {

            min = result[i].fitnessValue;
            leastfittestParent = result[i];
        }

    }

    return leastfittestParent;
}

var getSecondLeastFittestParent = function (population, leastfittestParent) {

    let result = JSON.parse(JSON.stringify(population))
    let min = result[0].fitnessValue;
    let secondLeastFittestParent = result[0];


    for (let i = 0; i < result.length; i++) {

        if (leastfittestParent.id != result[i].id) {

            if (result[i].fitnessValue < min) {

                min = result[i].fitnessValue;
                secondLeastFittestParent = result[i];
            }
        }

    }
    return secondLeastFittestParent;
};



var fitnessBaseDSelection = function (resultJSON, fittestParent, secondFittestParent, leastfittestParent, secondLeastFittestParent) {

    let result = JSON.parse(JSON.stringify(resultJSON))
    for (let i = 0; i < result.length; i++) {



        if (result[i].id == leastfittestParent.id) {

            result[i].busAllocation = fittestParent.busAllocation;

            //result id  replace as leastparent id
            result[i].fitnessValue = fittestParent.fitnessValue

        }
        if (result[i].id == secondLeastFittestParent.id) {

            result[i].busAllocation = secondFittestParent.busAllocation;
            //result id  replace as second leastparent id
            result[i].fitnessValue = secondFittestParent.fitnessValue

        }

    }

    // return newGenerationJson;
    return resultJSON;
};

var crossover = function (fittestParent, secondFittestParent, generation, crossRate) {

    let crossOverPoint = Math.floor(Math.random() * NO_OF_SLOTS);
    let weight = 0.4;
    let childArray = [];
    let sum = 0;

    if (Math.random(1).toFixed(2) < crossRate) {


        for (let i = 0; i < NO_OF_SLOTS; i++) {

            let childVAlue = 0;
            let x = fittestParent.busAllocation[i];
            let y = secondFittestParent.busAllocation[i];

            childVAlue = weight * x + (1 - weight) * y;

            // sum += Math.ceil(childVAlue);

            // if (sum >= NO_OF_BUSSES - 1) {
            //     childVAlue = Math.floor(childVAlue);
            // }
            // else {
            //     childVAlue = Math.ceil(childVAlue);
            // }

            childArray.push(Math.floor(childVAlue));


        }
        fittestParent.busAllocation = childArray;
        secondFittestParent.busAllocation = childArray;


    }

    for (var index = 0; index < generation.length; index++) {


        if (generation[index].id == fittestParent.id) {

            generation[index] = fittestParent;
        }
        if (generation[index].id == secondFittestParent.id) {

            generation[index] = secondFittestParent;
        }

    }
    return generation;

};

var crossover_2 = function (leastfitParent, secLeastfitParent, generation, crossRate) {

    let crossOverPoint = Math.floor(Math.random() * NO_OF_SLOTS);
    let weight = 0.4;
    let childArray = [];
    let sum = 0;

    if (Math.random(1).toFixed(2) < crossRate) {


        for (let i = 0; i < NO_OF_SLOTS; i++) {

            let childVAlue = 0;
            let x = leastfitParent.busAllocation[i];
            let y = secLeastfitParent.busAllocation[i];

            childVAlue = weight * x + (1 - weight) * y;

            sum += Math.ceil(childVAlue);

            if (sum >= NO_OF_BUSSES - 1) {
                childVAlue = Math.floor(childVAlue);
            }
            else {
                childVAlue = Math.ceil(childVAlue);
            }

            childArray.push(childVAlue);


        }
        leastfitParent.busAllocation = childArray;
        secLeastfitParent.busAllocation = childArray;


    }

    for (var index = 0; index < generation.length; index++) {


        if (generation[index].id == leastfitParent.id) {

            generation[index] = leastfitParent;
        }
        if (generation[index].id == secLeastfitParent.id) {

            generation[index] = secLeastfitParent;
        }

    }
    return generation;

};

var crossover_3 = function (leastfitParent, secondLeastFittestParent, generation, crossRate) {

    let crossOverPoint = Math.floor(Math.random() * NO_OF_SLOTS);
    let weight = 0.4;
    let childArray = [];
    let sum = 0;

    if (Math.random(1).toFixed(2) < crossRate) {


        let newsolution1 = chromosome(NO_OF_SLOTS, NO_OF_BUSSES);
        let newsolution2 = chromosome(NO_OF_SLOTS, NO_OF_BUSSES);
        while (JSON.stringify(leastfitParent.busAllocation) == JSON.stringify(newsolution1)) {

            newsolution1 = chromosome(NO_OF_SLOTS, NO_OF_BUSSES);
        }


        leastfitParent.busAllocation = newsolution1;
        secondLeastFittestParent.busAllocation = newsolution2;



    }

    for (var index = 0; index < generation.length; index++) {


        if (generation[index].id == leastfitParent.id) {

            generation[index] = leastfitParent;
        }

    }
    return generation;

};

var mutate = function (newGenerationJson, muRate) {
    let resultAllocation = JSON.parse(JSON.stringify(newGenerationJson));
    // let mutationPointOne  = Math.floor(Math.random() * population[0].length);
    // let mutationPointTwo  = Math.floor(Math.random() * population[0].length);
    const mutationPointOne = Math.floor(Math.random() * NO_OF_SLOTS);
    const mutationPointTwo = Math.floor(Math.random() * NO_OF_SLOTS);

    // let randomNum=Math.random(1).toFixed(2);


    if (Math.random(1).toFixed(2) < muRate) {

        const randomAllocation = Math.floor(Math.random() * resultAllocation.length);
        let muteAllocation = resultAllocation[randomAllocation].busAllocation;

        const temp = muteAllocation[mutationPointOne];
        // console.log('TEMP VALUE: ', temp);

        muteAllocation[mutationPointOne] = muteAllocation[mutationPointTwo];
        muteAllocation[mutationPointTwo] = temp;




        for (var index = 0; index < newGenerationJson.length; index++) {


            if (newGenerationJson[index].id == muteAllocation.id) {

                newGenerationJson[index] = muteAllocation;
            }
        }


        return newGenerationJson;

    }
    return newGenerationJson;

}


var calculateFitness = function (newGenerationJson, busArray, passengerAverage) {
    // console.log("-------new generation after calculate-------");
    // console.log(newGenerationJson);

    id = 0;
    let solutionNewGen = JSON.parse(JSON.stringify(newGenerationJson))
    // console.log('--------SOlution change bus allocation ------', solutionNewGen[0].busAllocation);

    for (let i = 0; i < solutionNewGen.length; i++) {


        let returnEvaluvateSolution = calcCost(solutionNewGen[i].busAllocation, passengerAverage, busArray)

        solutionNewGen[i] = returnEvaluvateSolution;

    }

    return solutionNewGen;
}
var getFittestLastSolution = function (population, fittestSolution) {

    let result = JSON.parse(JSON.stringify(population));

    let max = result[0].fitnessValue;
    let fittestParent = result[0];
    for (let i = 0; i < result.length; i++) {

        if (result[i].fitnessValue > max) {

            max = result[i].fitnessValue;
            fittestParent = result[i];
        }

    }
    if (fittestSolution.fitnessValue > fittestParent.fitnessValue) {

        return fittestSolution;
    } else {

        return fittestParent;
    }
}

function geneticAlgorithm(config, predictedJson, callback) {
    returnJson = []
    busService.findBusByRoute({ body: { routeNo: config.route_no } }, '', function (data) {
        busArray = data.data;
        predictedJson.paramters.forEach((element, i) => {
            returnJson.push(generateSchedule(new Date("October 13, 2014 " + element.startTime), new Date("October 13, 2014 " + element.endTime), element.fixedInterval, element.noOfBusses, element.passengerAverage, busArray, config.population, config.generation, config.mutation, config.crossover))
            returnJson[i].startTime = element.startTime
            returnJson[i].endTime = element.endTime
            returnJson[i].fixedInterval = element.fixedInterval
            returnJson[i].noOfBusses = element.noOfBusses
        })
        callback(returnJson)
    })
}

// console.log(generateSchedule(startTime, endTime, 20, 8, [73, 68, 47, 40, 48], busArray, 15, 300, 0.01, 0.6));
function generateSchedule(startTime, endTime, fixedInterval, noOfBuses, passengerAverage, busArray, popSize, maxGenCount, muRate, crossRate) {

    startTime = startTime
    endTime = endTime
    let totalTime = getHourDifference(startTime, endTime)
    let noOfSlots = Math.round(totalTime / fixedInterval)


    let generationJson = initialPopulation(popSize, noOfSlots, noOfBuses, passengerAverage, busArray);
    // console.log('@@##@#@#@# POPULATION @#@#@#@#', JSON.stringify(generationJson));
    let GenerationCount = 0;
    // let newFit;
    let gjson;
    // let newGeneration;
    let fitparent;
    let fitSolutionSoFar;

    while (GenerationCount != maxGenCount) {

        fitparent = getFittestParent(generationJson);
        let secondfitParent = getSecondFittestParent(generationJson, fitparent);
        let leastfitParent = getLeastFittestParent(generationJson);
        let secLeastfitParent = getSecondLeastFittestParent(generationJson, leastfitParent);
        generationJson = fitnessBaseDSelection(generationJson, fitparent, secondfitParent, leastfitParent, secLeastfitParent);
        // let crossGen = crossover(fitparent, secondfitParent, generationJson);
        // crossGen =crossover_2(leastfitParent, secLeastfitParent,generationJson);
        let crossGen = crossover_3(fitparent, secLeastfitParent, generationJson, crossRate);
        gjson = mutate(crossGen, muRate);
        generationJson = calculateFitness(gjson, busArray, passengerAverage);
        fitSolutionSoFar = getFittestLastSolution(generationJson, fitparent);
        GenerationCount++



    }
    return fitSolutionSoFar;

}

module.exports = { geneticAlgorithm };
