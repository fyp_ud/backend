const busService = require('../../bus/busService')
const config = require('../../../config/config')
const scheduleService = require('../../bus-allocation/scheduleService')

var busArray = []
var returnJSON = {
    allocation: {
        busAllocation: [],
        busNo: [],
        time_slot: "",
        busIDs: [],
        fixedInterval: "",
        numberOfBusses: "",
        passengerAverage: []
    },
    message: "",
    fitnessValue: 0
};

var date = new Date()
/*
* main function that will be called from schedule service
*/
function generateDailyAllocation(config, predictedJson, callback) {
    var output = [];
    let inputJSON = predictedJson;
    let schedule = {
        allocation: [],
        route: config.route_no
    }
    busService.bulkUpdateBus("", '', function (data) {
        busService.findBusByRoute({ body: { routeNo: config.route_no } }, '', function (data) {
            busArray = data.data;
            let newJSON;
            inputJSON.paramters.forEach(element => {
                busArray = busArray.sort(function (obj1, obj2) {
                    return obj1.totalRevenue - obj2.totalRevenue;
                });

                //Pass paramters to the ILS algorithm to geneate a solution for te given inputs
                newJSON = JSON.parse(JSON.stringify(generateSchedule(new Date("October 13, 2014 " + element.startTime), new Date("October 13, 2014 " + element.endTime), /*Fixed Interval*/element.fixedInterval, /*Number of busses*/element.noOfBusses, /*Passenger Average*/element.passengerAverage, busArray, /*Number of iterations*/config.iterationCount)))
                output.push(newJSON)
                newJSON.allocation.busNo.forEach(returnElement => {
                    busArray.forEach(function (busElement, i) {
                        if (busElement.busNo == returnElement.busIDs) {
                            busArray[i].totalRevenue ? busArray[i].totalRevenue = returnElement.totalRevenue + busArray[i].totalRevenue : busArray[i].totalRevenue = returnElement.totalRevenue
                        }
                    });
                });
            });

            //Updating revenue of each bus after an allocation for a time slot is finished
            busArray.forEach(element => {
                busService.updateBus({ body: element }, '', function (data) {
                })
            });

            //Save schedule into the database which is recived from the algorithm
            output.forEach(function (element, i) {
                schedule.allocation.push(
                    {
                        busNumbers: element.allocation.busIDs,
                        time_slot: element.allocation.time_slot,
                        allocation: element.allocation.busAllocation,
                        fixedInterval: element.allocation.fixedInterval,
                        numberOfBusses: element.allocation.numberOfBusses,
                        passengerAverage: element.allocation.passengerAverage,
                        message: element.allocation.message
                    }
                )
            });
            callback(schedule)
        })
    })

}

function generateSchedule(startTime, endTime, fixedInterval, noOfBusses, avgPassengerCount, busArray, maxIterationCount) {
    returnJSON.allocation.time_slot = startTime.toString().split(" GMT")[0].split(" ")[4] + " - " + endTime.toString().split(" GMT")[0].split(" ")[4]
    returnJSON.allocation.numberOfBusses = noOfBusses;
    returnJSON.allocation.fixedInterval = fixedInterval;
    returnJSON.allocation.passengerAverage = avgPassengerCount;
    returnJSON.allocation.message = '';
    var globalBest = -100;
    let allSolutions = [];
    let totalTime = getHourDifference(startTime, endTime)
    let isValidInitialSolution = true;
    let initialSolution = []
    let noOfSlots = Math.round(totalTime / fixedInterval);
    let count = 0;
    if (noOfBusses < noOfSlots) {
        returnJSON.allocation.message = "Number of busses are not enough to generate the schedule atleast " + noOfSlots + " busses are needed";
        return returnJSON;
    } else if (noOfSlots > avgPassengerCount.length) {
        returnJSON.allocation.message = "Passenger average array is not enough to cover an optimal output\nNo of Slots : " + noOfSlots + "\nPassenger Average Array : " + avgPassengerCount.length;
        return returnJSON;
    }
    //Generate solutions and evaluvate untill maxiteration count exceed
    for (let r = 0; r < maxIterationCount; r++) {
        let previousGlobalSolution = JSON.parse(JSON.stringify(returnJSON.allocation.busAllocation));
        let validationCount = 0;
        do {
            //Intialize a new soluion
            initialSolution = generateInitialSolution(noOfSlots, noOfBusses);
            //Validating whether the generated solutions is arleady evaluvated or not
            outerloop: for (let i = 0; i < allSolutions.length; i++) {
                validationCount = validationCount + 1

                //Statement becomes true if all the possible solutions are already generated
                if (validationCount > allSolutions.length + config.allSolutionLength) {
                    if (globalBest <= 0) {
                        // Generated solution is optimal for the given input But the number of busses are more than enough
                        returnJSON.allocation.message = "Allocated buses are more then the actual need"
                        return returnJSON
                    } else {
                        return returnJSON
                    }
                }

                //Break from the loop and generate another solutions if the current solutions is already beign generated 
                if (allSolutions[i].toString() == initialSolution.toString()) {
                    isValidInitialSolution = false;
                    break outerloop;
                }
                //Change isValidInitialSolution true in order to break from the loop
                if (isValidInitialSolution == false) {
                    isValidInitialSolution = true;
                }
            }
        } while (!isValidInitialSolution);

        //Adding the current solutions to the all solution array(pool)
        allSolutions.push(initialSolution);
        //Swap elements and evaluvate
        allSolutions.push(swapSolutionElements(initialSolution, avgPassengerCount, busArray, globalBest));
        if (returnJSON.allocation.busAllocation.toString() == previousGlobalSolution.toString()) {
            count++;
        } else {
            count = 0;
        }
        if (count > (maxIterationCount / 2)) {
            if (globalBest <= 0) {
                returnJSON.allocation.message = "Maximum iteration has been reached"
                return returnJSON
            } else {
                return returnJSON
            }
        }
    }

    if (globalBest <= 0) {
        returnJSON.allocation.message = "Solution is not optimal"
        return returnJSON
    } else {
        return returnJSON
    }
}

function getHourDifference(dt1, dt2) {
    let diff = dt2 - dt1;
    return Math.floor((diff / 1000) / 60);
}

function generateInitialSolution(noOfSlots, noOfBusses) {

    let solution = []
    var maxValue = (noOfBusses - noOfSlots) + 1;
    let minValue = 1;
    let currentSum = 0;
    let randomValue = 0;
    let isValidRandom;


    for (let i = noOfSlots; i > 0; i--) {
        let sumOfCurrentSolution = solution.reduce((a, b) => a + b, 0);
        let maxRandomForCurrrentIteration = noOfBusses - sumOfCurrentSolution - i + 1;
        if (i == 1) {
            randomValue = maxRandomForCurrrentIteration;
        } else {
            do {
                randomValue = Math.floor((Math.random() * maxValue) + minValue)
                isValidRandom = (maxRandomForCurrrentIteration >= randomValue)
            } while (!isValidRandom);
        }
        do {
            isValidRandom = validateRandomValue((currentSum + randomValue), noOfBusses)
        }
        while (!isValidRandom);
        currentSum = currentSum + randomValue;
        solution.push(randomValue);

    }

    return solution;

}


function validateRandomValue(currentSum, noOfBusses) {
    return currentSum <= noOfBusses;
}


function swapSolutionElements(originalSolution, passengerAverage, busArray, funcGlobalBest) {
    for (let i = 0; i < originalSolution.length; i++) {
        let solution = JSON.parse(JSON.stringify(originalSolution));
        for (let r = i; r < originalSolution.length; r++) {
            let returnEvaluvateSolution = evaluvateSolution(solution, passengerAverage, busArray);
            fitnessValue = returnEvaluvateSolution.fitnessValue;
            if (funcGlobalBest == fitnessValue) {
                returnJSON.allocation.busAllocation = JSON.parse(JSON.stringify(solution));
                returnJSON.allocation.busNo = returnEvaluvateSolution.busNo
                returnJSON.allocation.busIDs = returnEvaluvateSolution.busIDs
                returnJSON.fitnessValue = fitnessValue
            }
            if (funcGlobalBest < fitnessValue) {
                funcGlobalBest = fitnessValue;
                returnJSON.allocation.busAllocation = JSON.parse(JSON.stringify(solution));
                returnJSON.allocation.busNo = returnEvaluvateSolution.busNo
                returnJSON.allocation.busIDs = returnEvaluvateSolution.busIDs
                returnJSON.fitnessValue = fitnessValue
            }
            if (r != solution.length - 1) {
                let temp = solution[i];
                solution[i] = solution[r + 1];
                solution[r + 1] = temp;
            }
        }
    }
    return returnJSON.allocation.busAllocation;
}


function evaluvateSolution(solution, passengerAverage, busArray) {
    let spliceIndex = 0;
    let isEnoughSeats = false;
    let returnEvaluvateJSON = {
        busNo: [],
        fitnessValue: 0,
        busIDs: []
    }

    for (let i = 0; i < solution.length; i++) {
        let sumOfSeats = 0;
        let spliceArray = JSON.parse(JSON.stringify(busArray));
        let newBusArray = spliceArray.splice(spliceIndex, solution[i])
        spliceIndex = spliceIndex + solution[i];
        newBusArray = JSON.parse(JSON.stringify(newBusArray));
        let totalRevenue = passengerAverage[i] / newBusArray.length

        for (let r = 0; r < newBusArray.length; r++) {
            sumOfSeats = sumOfSeats + newBusArray[r].noOfSeats;
            returnEvaluvateJSON.busNo.push({ totalRevenue: totalRevenue, busNo: newBusArray[r].busNo })
            if (newBusArray.length == 1) {
                returnEvaluvateJSON.busIDs[i] = " [ " + newBusArray[r].busNo + " ] ";
            } else if (r == 0) {
                returnEvaluvateJSON.busIDs[i] = " [ " + newBusArray[r].busNo;
            } else if (r == newBusArray.length - 1) {
                returnEvaluvateJSON.busIDs[i] += " , " + newBusArray[r].busNo + " ] ";
            } else {
                returnEvaluvateJSON.busIDs[i] += " , " + newBusArray[r].busNo;
            }
        }
        let slack = sumOfSeats - passengerAverage[i];
        if (slack >= 0) {
            isEnoughSeats = true;
        } else {
            isEnoughSeats = false;
        }

        if (slack < -10) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.5;
        } else if (slack < 0) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 1;
        } else if (slack > 100) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 5.2;
        } else if (slack > 90) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 4.1;
        } else if (slack > 80) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 3;
        } else if (slack > 70) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 1.9;
        } else if (slack > 60) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.8;
        } else if (slack > 50) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.7;
        } else if (slack > 40) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.6;
        } else if (slack > 30) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.5;
        } else if (slack > 20) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0;
        } else if (slack > 5) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 1.5;
        } else if (slack > 0) {
            returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 1;
        }

        // Evaluvating whether the busses are divided in a proper way if the number of busses are more than the actual need
        for (let k = 0; k < passengerAverage.length - 1; k++) {
            for (let j = 0; j < passengerAverage.length; j++) {
                if (passengerAverage[k] > passengerAverage[j]) {
                    if (solution[k] < solution[j]) {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.5
                    } else {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.05
                    }
                } else {
                    if (solution[k] > solution[j]) {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue - 0.5
                    } else {
                        returnEvaluvateJSON.fitnessValue = returnEvaluvateJSON.fitnessValue + 0.05
                    }
                }

            }
        }
    }
    return returnEvaluvateJSON;
}



module.exports = { generateSchedule, generateDailyAllocation };