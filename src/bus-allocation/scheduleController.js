'use strict'

//Import Required modules
const scheduleService = require('./scheduleService');
const response = require('../../services/responseService');
var exportMethods = {};

/**
 * get all buses from the system
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getSchedule = function (req, res) {
    scheduleService.getSchedule(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * create schedule
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.createSchedule = function (req, res) {
    scheduleService.createSchedule(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    });
}

module.exports = exportMethods;