//import validator class
const joi = require('joi');

//user login Schema and validations to be done 
module.exports.login = joi.object().keys({
    email: joi.string().email().required(),
    password: joi.required()
});

//user registration Schema and validations to be done 
module.exports.newUser = joi.object().keys({
    email: joi.string().email().required(),
    role: joi.string(),
    password: joi.required(),
    busNo: joi.string(),
    route: joi.string(),
    capacity: joi.number(),
    contact_no: joi.number().min(10).required(),
    first_name: joi.string().required(),
    last_name: joi.string().required(),
    bus_id: joi.string().alphanum().min(24).max(24),
});
//user password change Schema and validations to be done 
module.exports.changePassword = joi.object().keys({
    email: joi.string().email().required(),
    oldPassword: joi.required(),
    newPassword: joi.required()
});

//user_id Schema and validations to be done 
module.exports.user_id = joi.object().keys({
    user_id: joi.string().alphanum().min(24).max(24).required(),
});

//user update schema 
module.exports.updateUser = joi.object().keys({
    user_id: joi.string().alphanum().min(24).max(24).required(),
    email: joi.string().email(),
    role: joi.number().integer().min(1).max(10),
    busNo: joi.string(),
    route: joi.string(),
    capacity: joi.number(),
    contact_no: joi.number().min(10),
    first_name: joi.string(),
    last_name: joi.string(),
    bus_id: joi.string().alphanum().min(24).max(24),
})
