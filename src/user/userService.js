'use strict'
//Imports
const modelUser = require('./userModel').user;
const userModelMethods = require('./userModel').exportMethods;
const callBackResponse = require('../../services/callBackResponseService');
var generatePassword = require('password-generator');
const activeStatus = "Active";
const deactiveStatus = "Deactivate"

/**
 * create new user
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.newUser = function (req, res, callBack) {
    const user = new modelUser();
    user.email = req.body.email;
    user.role = req.body.role;
    user.busNo = req.body.busNo;
    user.milage = req.body.milage;
    user.route = req.body.route;
    user.capacity = req.body.capacity;
    user.status = activeStatus;
    user.bus_id = req.body.bus_id;
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.contact_no = req.body.contact_no;
    user.setPassword(req.body.password);
    userModelMethods.find({ email: req.body.email, status: activeStatus }, { password: 0, salt: 0 }, function (error, UserRet) {
        if (error) {
            callBack(callBackResponse.callbackWithfalseMessage(error));
        }
        else if (UserRet == null || UserRet == undefined || UserRet.length == 0) {
            //add user to the database
            userModelMethods.saveUser(user, function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithfalseMessage(err));
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('Registration successful'));
                }
            });
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('Email Already Exist'));
        }
    });
}


/**
 * user login
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.login = function (req, res, callBack) {
    userModelMethods.findOne({ email: req.body.email, status: activeStatus }, {}, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid user Name'));
        }
        else {
            const status = user.validPassword(req.body.password);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Invalid Password'));
            } else {
                // const data = { bus_id: user.bus_id, role: user.role, email: user.email, id: user._id, busNo: user.busNo, route: user.route, capacity: user.capacity, milage: user.milage };
                callBack(callBackResponse.callbackWithData(user));
            }
        }
    });
}

/**
 * change password
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.changePassword = function (req, res, callBack) {
    userModelMethods.findOne({ email: req.body.email, status: activeStatus }, {}, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid user Name'));
        }
        else {
            const status = user.validPassword(req.body.oldPassword);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Current Password is Incorrect'));
            }
            user.setPassword(req.body.newPassword);
            userModelMethods.saveUser(user, function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('sucessfully changed the password'));
                }
            });
        }
    });
}

/**
 * get user by id
 * @param {*} user_id 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getUserByID = function (user_id, res, callBack) {
    userModelMethods.findByID(user_id, { password: 0, salt: 0 }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid User ID'));
        }
        else {
            callBack(callBackResponse.callbackWithData(user));
        }
    });
}

/**
 * delete user from the database
 * @param {*} user_id 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.removeUser = function (user_id, res, callBack) {
    userModelMethods.fincByIdandUpdate({ _id: user_id }, { status: deactiveStatus }, { safe: true }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('User Not Found'));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('User Removed From The System'));
        }
    });
};

/**
 * get Active users from the database
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getActiveUsers = function (req, res, callBack) {
    userModelMethods.find({ status: activeStatus }, { password: 0, salt: 0 }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (user == null || user == undefined, user.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage('No Users Found'));
        }
        else {
            callBack(callBackResponse.callbackWithData(user));
        }
    });
}

/**
 * update user from the database
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.updateUser = function (body, res, callBack) {
    var user = JSON.parse(JSON.stringify(body));
    delete user['user_id']
    userModelMethods.fincByIdandUpdate(body.user_id, user, { new: true, safe: true }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('No Users Found'));
        }
        else {
            callBack(callBackResponse.callbackWithData(user));
        }
    });
}