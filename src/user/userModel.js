const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};
//create user Schema 
const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true
  },
  salt: String,
  busNo: String,
  route: String,
  bus_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Bus' },
  capacity: Number,
  milage: Number,
  first_name: String,
  last_name: String,
  contact_no: Number,
  status: String,
}, { timestamps: true });

//validate pasword with the database
UserSchema.methods.validPassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.password === hash;
};
//encrypt the password before save
UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

var user = mongoose.model('User', UserSchema);

exportMethods.saveUser = (newUser, callback) => {
  newUser.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
  user.findOne(query, options).populate('bus_id').exec(callback);
}

exportMethods.find = (query, options, callback) => {
  user.find(query, options).populate('bus_id').exec(callback);
};

exportMethods.findByID = (query, options, callback) => {
  user.findById(query, options).populate('bus_id').exec(callback);
};

exportMethods.fincByIdandUpdate = (query, userInfo, options, callback) => {
  user.findByIdAndUpdate(query, userInfo, options, callback);
}

module.exports = {
  exportMethods,
  user
}

