'use strict'
//Import Required Models
const modelUser = require('./userModel');
const userService = require('./userService');
const response = require('../../services/responseService');

/**
 * Register new user to the system
 * @param {*} req email , password , role
 * @param {*} res 
 */
module.exports.newUser = function (req, res) {
    userService.newUser(req, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}
/**
 * user login to the system 
 * @param {*} req email , password 
 * @param {*} res 
 */
module.exports.login = function (req, res) {
    userService.login(req, res, function (data) {
        if (data.status) {
            return response.successTokenWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * change current password of the user
 * @param {*} req email , oldPassword , newPassword
 * @param {*} res 
 */
module.exports.changePassword = function (req, res) {
    userService.changePassword(req, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    })
}
/**
 * get user by user ID
 * @param {*} req user_id
 * @param {*} res 
 */
module.exports.getUserById = function (req, res) {
    userService.getUserByID(req.body.user_id, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    })
}

/**
 * Remove user by user ID
 * @param {*} req user_id
 * @param {*} res 
 */
module.exports.RemoveUser = function (req, res) {
    userService.removeUser(req.body.user_id, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    })
}

/**
 * get users from the database
 * @param {*} req 
 * @param {*} res 
 */
module.exports.getUsers = function (req, res) {
    userService.getActiveUsers(req.body.user_id, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    })
}

/**
 * update user from the database
 * @param {*} req 
 * @param {*} res 
 */
module.exports.updateUser = function (req, res) {
    userService.updateUser(req.body, res, function (data) {       
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    })
}