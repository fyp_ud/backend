'use strict'
//Import Required Models
const modelPassenger = require('./passengerModel');
const passengerService = require('./passengerService');
const response = require('../../services/responseService');

/**
 * Register new passenger to the system
 * @param {*} req email , password , role
 * @param {*} res 
 */

module.exports.signUp = function (req, res) {
    passengerService.signUp(req, res, function (data) {
        if (data.status) {
            return response.successTokenWithData(data.data,res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * passenger signin to the system 
 * @param {*} req email , password 
 * @param {*} res 
 */
module.exports.signIn = function (req, res) {
    passengerService.signIn(req, res, function (data) {
        if (data.status) {
            return response.successTokenWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * change current password of the passenger
 * @param {*} req email , oldPassword , newPassword
 * @param {*} res 
 */
module.exports.changePassword = function (req, res) {
    passengerService.changePassword(req, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    })
}

/**
 * update passenger from the database
 * @param {*} req 
 * @param {*} res 
 */
module.exports.updatePassenger = function (req, res) {
    passengerService.updateUser(req.body, res, function (data) {       
        if (data.status) {
            return response.successWithData(data.data, res);
        } else {
            return response.customError(data.data, res);
        }
    })
}