const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

/** 
* Create passenger schema
*/

const PassengerSchema = new Schema({
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    salt: String,
    name: String,
    contact: Number,
    status: String,
  }, { timestamps: true });

  /**
  * validate pasword with the database
  */
  PassengerSchema.methods.validPassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.password === hash;
  };
  /**
   * encrypt the password before save
  */
  PassengerSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  };

  var passenger = mongoose.model('Passenger', PassengerSchema);

  exportMethods.savePassenger = (newPassenger, callback) => {
    newPassenger.save(callback);
  }
  
  exportMethods.findOne = (query, options, callback) => {
    passenger.findOne(query, options).exec(callback);
  }
  
  exportMethods.find = (query, options, callback) => {
    passenger.find(query, options).exec(callback);
  };
  
  exportMethods.findByID = (query, options, callback) => {
    passenger.findById(query, options).exec(callback);
  };
  
  exportMethods.fincByIdandUpdate = (query, passengerInfo, options, callback) => {
    passenger.findByIdAndUpdate(query, passengerInfo, options, callback);
  }
  
  module.exports = {
    exportMethods,
    passenger
  }