const joi = require('joi');

/**
* passenger signIn Schema and validations to be done 
*/
module.exports.signIn = joi.object().keys({
    email: joi.string().email().required(),
    password: joi.required()
});

/**
* passenger sign up Schema and validations to be done 
*/
module.exports.signUp = joi.object().keys({
    email: joi.string().email().required(),
    password: joi.required(),
    contact: joi.number().min(10).required(),
    name: joi.string().required()
});
/**
* passenger password change Schema and validations to be done 
*/
module.exports.changePassword = joi.object().keys({
    email: joi.string().email().required(),
    oldPassword: joi.required(),
    newPassword: joi.required()
});

/**
* passenger update schema 
*/
module.exports.updatePassenger = joi.object().keys({
    id: joi.string().alphanum().min(24).max(24).required(),
    contact: joi.number().min(10),
    name: joi.string(),
})