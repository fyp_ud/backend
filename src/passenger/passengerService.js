'use strict'
//Imports
const modelPassenger = require('./passengerModel').passenger
const passengerModelMethods = require('./passengerModel').exportMethods;
const callBackResponse = require('../../services/callBackResponseService');
var generatePassword = require('password-generator');
const activeStatus = "Active";
const deactiveStatus = "Deactivate"

/**
 * create new passenger
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.signUp=function(req,res,callBack){
    const passenger=new modelPassenger();
    passenger.email=req.body.email;
    passenger.setPassword(req.body.password);
    passenger.name=req.body.name;
    passenger.contact = req.body.contact;
    passenger.status = activeStatus;
    passengerModelMethods.findOne({ email: req.body.email, status: activeStatus }, { password: 0, salt: 0  },function (error, PassengerRet){
        if (error) {
            callBack(callBackResponse.callbackWithfalseMessage(error));
        }else if (PassengerRet == null || PassengerRet == undefined || PassengerRet.length == 0) {
            /**
             * add passenger to the database
            */
            passengerModelMethods.savePassenger(passenger, function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithfalseMessage(err));
                } else {
                    passengerModelMethods.findOne({ email: req.body.email, status: activeStatus },{},function(error,passenger){
                        if(error){
                            callBack(callBackResponse.callbackWithfalseMessage(err));
                        }else{
                            callBack(callBackResponse.callbackWithData(passenger));
                        }
                    })
                }
            });
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('Email Already Exist'));
        }
    });
}

/**
 * passenger login
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.signIn = function (req, res, callBack) {
    passengerModelMethods.findOne({ email: req.body.email, status: activeStatus }, {}, function (err, passenger) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (passenger == null || passenger == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid user Name'));
        }
        else {
            const status = passenger.validPassword(req.body.password);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Invalid Password'));
            } else {
                callBack(callBackResponse.callbackWithData(passenger));
            }
        }
    });
}

/**
 * change password
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.changePassword = function (req, res, callBack) {
    passengerModelMethods.findOne({ email: req.body.email, status: activeStatus }, {}, function (err, passenger) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (passenger == null || passenger == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid User Name'));
        }
        else {
            const status = passenger.validPassword(req.body.oldPassword);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Current Password is Incorrect'));
            }
            passenger.setPassword(req.body.newPassword);
            passengerModelMethods.savePassenger(passenger, function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('Sucessfully changed the password'));
                }
            });
        }
    });
}

/**
 * update passenger from the database
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.updateUser = function (body, res, callBack) {
    var passenger = JSON.parse(JSON.stringify(body)||'{}');
    delete passenger['id']
    passengerModelMethods.fincByIdandUpdate(body.id, passenger, { new: true, safe: true }, function (err, passenger) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (passenger == null || passenger == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('No Passenger Found'));
        }
        else {
            callBack(callBackResponse.callbackWithData(passenger));
        }
    });
}