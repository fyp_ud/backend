const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

//create user Schema 
const locationRawData = new Schema({
  startTime: String,
  endTime: String,
  speed: String,
  bus: Schema.Types.Mixed,
  totalTime: String,
  tracker: Schema.Types.Mixed,
});

var journey = mongoose.model('journeyStatus', locationRawData);

exportMethods.saveJourney = (newJourney, callback) => {
  newJourney.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
  journey.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
  journey.find(query, options, callback);
};

exportMethods.findByID = (query, options, callback) => {
  journey.findById(query, options, callback);
};

exportMethods.fincByIdandUpdate = (query, journeyInfo, options, callback) => {
  journey.findByIdAndUpdate(query, journeyInfo, options, callback);
}

module.exports = {
  exportMethods,
  journey
}