'use strict'
//Import Required Models
const journeyModel = require('./journeyModel');
const journeyService = require('./journeyService');
const response = require('../../../services/responseService');

/**
 * add continous locations to the data base
 * @param {*} req bus , starTime 
 * @param {*} res 
 */
module.exports.startJourney = function (req, res) {
    journeyService.startJourney(req.body,res,function(data){
        if(data.status){
            return response.successWithData(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}

/**
 * get data according the bus_id and the time stamp
 * @param {*} req journey_id , endTime
 * @param {*} res 
 */
module.exports.journeyEnd = function (req, res) {
    journeyService.journeyEnd(req,res,function(data){
        if(data.status){
            return response.successWithMessage(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}