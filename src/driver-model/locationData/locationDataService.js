'use strict'
//Imports
const locationDataModel = require('./locationDataModel').cordinate;
const locationDataMethods = require('./locationDataModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');

/**
 * add new cordinates to the system
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.newCordinate = function (body, res, callBack) {
    const cordinate = new locationDataModel();
    cordinate.longitude = body.longitude;
    cordinate.latitude = body.latitude;
    cordinate.tracker_id = body.tracker_id;
    cordinate.bus_id = body.bus;
    locationDataMethods.saveBusRoute(cordinate, function (err) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('cordinate saved'));
        }
    });
}

/**
 * get raw cordinates according to the data
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getRawCordinates = function (body, res, callBack) {
    locationDataMethods.find({ bus_id: body.bus_id, updatedAt: { $lte: body.startDate } }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (data == undefined || data == null || data.length == 0) {
            callBack(callBackResponse.callbackWithSucessMessage('no cordinates found'));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}