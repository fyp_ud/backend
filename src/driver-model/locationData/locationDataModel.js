const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const exportMethods = {};

//create user Schema 
const locationRawData = new Schema({
  longitude: {
    type: String,
    required: true,
  },
  latitude: {
    type: String,
    required: true
  },
  bus_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  tracker_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'busTracker'
  }
}, { timestamps: true });

var cordinate = mongoose.model('busCordinates', locationRawData);

exportMethods.saveBusRoute = (newCordinate, callback) => {
  newCordinate.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
  cordinate.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
  cordinate.find(query, options, callback);
};

exportMethods.findByID = (query, options, callback) => {
  cordinate.findById(query, options, callback);
};

exportMethods.fincByIdandUpdate = (query, busCordinate, options, callback) => {
  cordinate.findByIdAndUpdate(query, busCordinate, options, callback);
}

module.exports = {
  cordinate,
  exportMethods
}
