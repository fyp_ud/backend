'use strict'
//Import Required Models
const locationDataservice = require('./locationDataService');
const response = require('../../../services/responseService');

/**
 * add continous locations to the data base
 * @param {*} req longitude , latitude , bus_id
 * @param {*} res 
 */
module.exports.newCordinate = function (req, res) {
    locationDataservice.newCordinate(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get data according the bus_id and the time stamp
 * @param {*} req bus_id , timeStamp
 * @param {*} res 
 */
module.exports.getRawCordinates = function (req, res) {
    locationDataservice.getRawCordinates(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}