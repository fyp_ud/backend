'use strict'

const trackingService = require('./trackingService');
const response = require('../../../services/responseService');

/**
 * add start location to the database
 * @param {*} req longitude , latitude , bus , status
 * @param {*} res 
 */
module.exports.newTracking = function (req, res) {
    trackingService.startTracking(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * update the location of the tracker
 * @param {*} req longitude , latiitude , status
 * @param {*} res 
 */
module.exports.updateTracker = function (req, res) {
    trackingService.trackingUpdate(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get all the active tracking devices
 * @param {*} req status
 * @param {*} res 
 */
module.exports.getTrackers = function (req, res) {
    trackingService.getActiveTrackers(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get all the active tracking devices
 * @param {*} req status
 * @param {*} res 
 */
module.exports.getRouteActiveTrackers = function (req, res) {
    trackingService.getActiveTrackerForRoute(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * check weather the bus have active tracker
 * @param {*} req status
 * @param {*} res 
 */
module.exports.checkActiveTracker = function (req, res) {
    trackingService.checkActiveTracker(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * remove tracker from the system
 * @param {*} req 
 * @param {*} res 
 */
module.exports.removeTracker = function (req, res) {
    trackingService.removeTracker(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get all the active tracking devices
 * @param {*} req status
 * @param {*} res 
 */
module.exports.getActiveTrackersByLocation = function (req, res) {
    trackingService.getActiveTrackersByLocation(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}
