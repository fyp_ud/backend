const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

//create user Schema 
const busTrackingSchema = new Schema({
  longitude: String,
  latitude: String,
  route_no: String,
  bus: { type: mongoose.Schema.Types.ObjectId, ref: 'Bus' },
  start_location: String,
  route_id: { type: mongoose.Schema.Types.ObjectId, ref: 'busRoute' },
  status: String
}, { timestamps: true });

var tracker = mongoose.model('busTracker', busTrackingSchema);

exportMethods.saveTracker = (newTracker, callback) => {
  newTracker.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
  tracker.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
  tracker.find(query, options, callback);
};

exportMethods.findByID = (query, options, callback) => {
  tracker.findById(query, options, callback);
};

exportMethods.fincByIdandUpdate = (query, trackerInfo, options, callback) => {
  tracker.findByIdAndUpdate(query, trackerInfo, options, callback);
}

exportMethods.findRoute = (query, options, callback) => {
  tracker.find(query, options).populate('route_id').exec(callback);
};

exportMethods.findAndPopulate = (query, options, callback) => {
  tracker.find(query, options).populate('bus').exec(callback);
};
module.exports = {
  exportMethods,
  tracker
}