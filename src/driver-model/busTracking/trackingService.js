'use strict'
//Imports
const trackingModel = require('./trackingModel').tracker;
const trackerModelMethods = require('./trackingModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactive";

/**
 * create new tracker
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.startTracking = function (body, res, callBack) {
    const tracker = new trackingModel();
    tracker.longitude = body.longitude;
    tracker.latitude = body.latitude;
    tracker.route_id = body.route_id;
    tracker.bus = body.bus;
    tracker.route_no = body.route_no;
    tracker.start_location = body.start_location;
    tracker.status = activeStatus;
    trackerModelMethods.saveTracker(tracker, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * update tracker
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.trackingUpdate = function (body, res, callBack) {
    var object = JSON.parse(JSON.stringify(body))
    delete object['tracker_id']
    trackerModelMethods.fincByIdandUpdate({ _id: body.tracker_id }, object, { "new": true }, function (err, track) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (track == null || track == undefined || track.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage(track));
        }
    })
}

/**
 * get trackers from the route
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getActiveTrackerForRoute = function (body, res, callBack) {
    trackerModelMethods.find({ status: activeStatus, 'bus.route': body.route }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * get all active trackers
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getActiveTrackers = function (body, res, callBack) {
    trackerModelMethods.find({ status: activeStatus }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * get tracker by bus id
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getTrackerByBusID = function (bus_id, res, callBack) {
    trackerModelMethods.findOne({ status: activeStatus, bus: bus_id }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * check active trackers for the bus
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.checkActiveTracker = function (body, res, callBack) {
    trackerModelMethods.findOne({ status: activeStatus, bus: body.bus_id }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * remove tracker from the system
 * @param {*} route_id 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.removeTracker = function (body, res, callBack) {
    trackerModelMethods.fincByIdandUpdate({ _id: body.tracker_id }, { status: deactiveStatus }, { safe: true }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Tracker Not Found'));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('Tracker Removed From The System'));
        }
    });
};

/**
 * get trackers from the route
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getActiveTrackersByLocation = function (body, res, callBack) {
    trackerModelMethods.findAndPopulate({ status: activeStatus, 'route_id': body.route_no, 'start_location': body.start_location }, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}