//import validator class
const joi = require('joi');

//newTracker Route Schema and validations to be done 
module.exports.newTracker = joi.object().keys({
    longitude: joi.string().required(),
    latitude: joi.number().required(),
    bus: joi.string().alphanum().min(24).max(24).required(),
});

//update Route Schema and validations to be done 
module.exports.updateTracker = joi.object().keys({
    tracker_id: joi.string().alphanum().min(24).max(24).required(),
    route_id: joi.string().alphanum().min(24).max(24).required(),
    longitude: joi.string(),
    latitude: joi.number(),
    bus: joi.string().alphanum().min(24).max(24)
});

//route Schema and validations to be done 
module.exports.route = joi.object().keys({
    route: joi.string().required()
});

//route_id Schema and validations to be done 
module.exports.trackerID = joi.object().keys({
    tracker_id: joi.string().alphanum().min(24).max(24).required(),
});


//route Schema and validations to be done for get trackers by location
module.exports.route = joi.object().keys({
    route_no: joi.string().required(),
    onGoingStatus:joi.string(),
    start_location:joi.string()
});

