'use strict'
//Import Required modules
const routeService = require('./emergencyService');
const response = require('../../../services/responseService');
const socketService = require('../../../services/socketService');

/**
 * add new bus emergency to the system
 * @param {*} req 
 * @param {*} res 
 */
module.exports.newEmergency = function (req, res) {
    routeService.newEmergency(req.body, res, function (data) {
        if (data.status) {
            socketService.emergency(data.data);
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}