//import validator class
const joi = require('joi');

//new Route Schema and validations to be done 
module.exports.newEmergency = joi.object().keys({
    bus_id: joi.string().alphanum().min(24).max(24).required(),
    route_id: joi.string().alphanum().min(24).max(24).required(),
    user_id: joi.string().alphanum().min(24).max(24).required()
});