'use strict'

//Imports
const emergencyServiceModel = require('./emergencyModel').emergency;
const emergencyMethods = require('./emergencyModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactivate"

/**
 * add new emergency to the system
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.newEmergency = function (body, res, callBack) {
    const emergency = new emergencyServiceModel();
    emergency.bus_id = body.bus_id;
    emergency.route_id = body.route_id;
    emergency.user_id = body.user_id;
    emergency.status = activeStatus;
    emergencyMethods.saveEmergency(emergency, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}
