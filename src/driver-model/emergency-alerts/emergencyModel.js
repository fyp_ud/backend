const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

//create Bus ROute Schema 
const emergencySchema = new Schema({
    bus_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Bus' },
    route_id: { type: mongoose.Schema.Types.ObjectId, ref: 'busRoute' },
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    status: String,
}, { timestamps: true });

var emergency = mongoose.model('emergency', emergencySchema);

exportMethods.saveEmergency = (newEmergency, callback) => {
    console.log(newEmergency)
    newEmergency.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
    emergency.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
    emergency.find(query, options, callback);
};

exportMethods.findByID = (query, options, callback) => {
    emergency.findById(query, options, callback);
};

exportMethods.fincByIdandUpdate = (query, emergencyInfo, options, callback) => {
    emergency.findByIdAndUpdate(query, emergencyInfo, options, callback);
}

module.exports = {
    exportMethods,
    emergency
}
