const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

//create Bus ROute Schema 
const busRouteSchema = new Schema({
  route_no: String,
  total_km: String,
  route_from:String,
  route_to:String,
  bus_stops: Schema.Types.Mixed,
  allocated_time: String,
  status: String,
}, { timestamps: true });

var busRoute = mongoose.model('busRoute', busRouteSchema);

exportMethods.saveBusRoute = (newBusRoute, callback) => {
  console.log(newBusRoute)
  newBusRoute.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
  busRoute.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
  busRoute.find(query, options, callback);
};

exportMethods.findByID = (query, options, callback) => {
  busRoute.findById(query, options, callback);
};

exportMethods.fincByIdandUpdate = (query, busRouteInfo, options, callback) => {
  busRoute.findByIdAndUpdate(query, busRouteInfo, options, callback);
}

module.exports = {
  exportMethods,
  busRoute
}
