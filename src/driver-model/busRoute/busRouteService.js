'use strict'

//Imports
const busRouteServiceModel = require('./busRouteModel').busRoute;
const busRouteMethods = require('./busRouteModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactivate"

/**
 * add new route to the system
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.newBusRoute = function (body, res, callBack) {
    const route = new busRouteServiceModel();
    route.route_no = body.route_no;
    route.total_km = body.total_km;
    route.bus_stops = body.bus_stops;
    route.route_from = body.route_from;
    route.route_to = body.route_to;
    route.allocated_time = body.allocated_time;
    route.status = activeStatus;
    busRouteMethods.saveBusRoute(route, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

/**
 * update bus route from the database
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.updateRoutes = function (body, res, callBack) {
    var object = JSON.parse(JSON.stringify(body));
    delete object['user_id'];
    busRouteMethods.fincByIdandUpdate({ _id: body.route_id }, object, { "new": true }, function (err, route) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (route == null || route == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Bus Route Not Found'));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage(route));
        }
    })
}

/**
 * get all routes from the database
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getAllRoutes = function (body, res, callBack) {
    busRouteMethods.find({ status: activeStatus }, {}, function (err, route) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (route == null || route == undefined || route.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage('Bus Route Not Found'));
        } else {
            callBack(callBackResponse.callbackWithData(route));
        }
    });
}

/**
 * find route by route_no
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.findByRouteNo = function (body, res, callBack) {
    busRouteMethods.find({ route_no: body.route_no, status: activeStatus }, {}, function (err, routes) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (routes == null || routes == undefined || routes.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage('Bus Route Not Found'));
        } else {
            callBack(callBackResponse.callbackWithData(routes));
        }
    });
}
/**
 * find bus routes using id
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.findByID = function (body, res, callBack) {
    //find route from database and Return
    busRouteMethods.findByID(body.route_id, {}, function (err, routes) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (routes == null || routes == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Bus Route Not Found'));
        } else {
            callBack(callBackResponse.callbackWithData(routes));
        }
    });
}

/**
 * remove route from the system
 * @param {*} user_id 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.removeroute = function (route_id, res, callBack) {
    busRouteMethods.fincByIdandUpdate({ _id: route_id }, { status: deactiveStatus }, { safe: true }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Bus Route Not Found'));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('Bus Route Removed From The System'));
        }
    });
};