//import validator class
const joi = require('joi');

//new Route Schema and validations to be done 
//route_no , total_km , bus_stops , allocated_time 
module.exports.newRoute = joi.object().keys({
    route_no: joi.string().required(),
    route_from: joi.string().required(),
    route_to: joi.string().required(),
    total_km: joi.number().required(),
    bus_stops: joi.array().required(),
    allocated_time: joi.string().required()
});

//edit Route Schema and validations to be done 
//route_id , route_no , total_km , bus_stops , allocated_time 
module.exports.editRoute = joi.object().keys({
    route_id: joi.string().alphanum().min(24).max(24).required(),
    route_no: joi.string().required(),
    route_from: joi.string().required(),
    route_to: joi.string().required(),
    total_km: joi.number().required(),
    bus_stops: joi.array().required(),
    allocated_time: joi.string().required()
});

//route_no Schema and validations to be done 
module.exports.route_no = joi.object().keys({
    route_no: joi.string().required()
});

//route_id Schema and validations to be done 
module.exports.route_id = joi.object().keys({
    route_id: joi.string().alphanum().min(24).max(24).required(),
});
