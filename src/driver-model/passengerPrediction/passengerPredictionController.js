'use strict'

//Import Required modules
const predictionService = require('./passengerPredictionService');
const response = require('../../../services/responseService');
var exportMethods = {};

/**
 * add new bus to the system
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getPrediction = function (req, res) {
    predictionService.getPrediction(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get predicted data from the system
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getPredictedData = function (req, res) {
    predictionService.getPredictedData(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

module.exports = exportMethods;