const request = require('request');
const config = require('../../../config/config');
const callBackResponse = require('../../../services/callBackResponseService');
const passengerModel = require('./passengerPredictionModel').predictionModel;
const passengerMethods = require('./passengerPredictionModel').exportMethods;
const date = require('date-and-time');
var dateStart = new Date('2018', '06' - 1, '01', '04', '00', '00');
const predictedData = require('../../../config/pastData');
const dateSlots = require('../../../config/timeSlots');
var timeIncrementMin = 5;

var options = {
    url: config.passengerPredictionAPI,
    headers: {
        'User-Agent': 'request',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(predictedData)
};

module.exports.getPrediction = (req, res, callBack) => {
    request.post(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var prediction = [];
            var predictionOut = JSON.parse(body)
            var passenger = new passengerModel();
            passenger.passengerCount = predictionOut.result;
            passengerMethods.savePrediction(passenger, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    // console.log(data)
                }
            })
            dateSlots.timeAllocations.paramters.forEach(element => {
                var from = 0;
                var to = from + (element.fixedInterval / 5);
                for (var i = 0; i < element.slots; i++) {
                    var arraySlice = predictionOut.result.splice(from, to)
                    var sum = 0;
                    arraySlice.forEach(count => {
                        sum = sum + count.passengerCount
                    });
                    element.passengerAverage.push(sum)
                    from = to;
                }
            });
            callBack(callBackResponse.callbackWithData(dateSlots.timeAllocations))
        }
        else {
            callBack(callBackResponse.callbackWithfalseMessage(error))
        }
    });
}

/**
 * get predicted data from the database according to the date
 * @param {*} body 
 * @param {*} res 
 * @param {*} callBack 
 */
module.exports.getPredictedData = function (body, res, callBack) {
    var getDate = new Date(body.start_date);
    var startDate = getDate;
    var next_date = new Date(getDate.setDate(getDate.getDate() + 1));
    passengerMethods.find({
        $and: [
            { "createdAt": { $gte: new Date(body.start_date) } },
            { "createdAt": { $lt: new Date(new Date(body.start_date).setDate(new Date(body.start_date).getDate() + 1)) } }
        ]
    }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithData(data))
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No Data Found"))
        } else {
            var processedData = [];
            data.forEach(predictions => {
                var prodData = [];
                var timeData = [];
                var realData = [];
                predictions.passengerCount.forEach(element => {
                    timeData.push(element.datetime);
                    prodData.push(element.passengerCount)
                });
                predictedData.yesterday_data.forEach(element => {
                    timeData.push(element.datetime);
                    realData.push(element.passengerCount)
                });
                processedData.push({
                    realData: { predictedData: realData, dateTime: timeData },
                    predictedData: { predictedData: prodData, dateTime: timeData }
                })
            });
            callBack(callBackResponse.callbackWithData(processedData))
        }
    })
}
