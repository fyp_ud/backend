//import validator class
const joi = require('joi');

//new Route Schema and validations to be done
module.exports.getPredictedData = joi.object().keys({
    start_date: joi.date().format('YYYY-MM-DD').required(),
});