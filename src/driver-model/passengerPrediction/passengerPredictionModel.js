'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

const predictionSchema = new Schema({
    passengerCount: [{
        datetime: Date,
        passengerCount: Number
    }]
}, { timestamps: true });

var predictionModel = mongoose.model("passenger_predictions", predictionSchema);

exportMethods.savePrediction = (newPrediction, callback) => {
    newPrediction.save(callback);
}

exportMethods.findOne = (query, callback) => {
    predictionModel.findOne(query).lean().exec(callback);
}

exportMethods.find = (query, callback) => {
    predictionModel.find(query).lean().exec(callback);
};

exportMethods.findByID = (query, callback) => {
    predictionModel.findById(query).lean().exec(callback);
};

exportMethods.fincByIdandUpdate = (query, predictionInfo, options, callback) => {
    predictionModel.findByIdAndUpdate(query, predictionInfo, options, callback);
}

module.exports = {
    exportMethods,
    predictionModel
}