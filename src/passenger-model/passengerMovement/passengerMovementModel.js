const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const exportMethods = {};

const passengerMovementSchema = new Schema({
    status : {
        type: Boolean,
        required: true,
    }
}, { timestamps: true });

var passengerMovement = mongoose.model('passengerMovement', passengerMovementSchema);

exportMethods.savePassengerMovement = (newPassengerMovement, callback) => {
    newPassengerMovement.save(callback);
  }
  
exportMethods.findOne = (query, options, callback) => {
    passengerMovement.findOne(query, options, callback);
  }
  
exportMethods.find = (query, options, callback) => {
    passengerMovement.find(query, options, callback);
  };
  
exportMethods.findByID = (query, options, callback) => {
    passengerMovement.findById(query, options, callback);
  };
  
exportMethods.fincByIdandUpdate = (query, passengerMovement, options, callback) => {
    passengerMovement.findByIdAndUpdate(query, passengerMovement, options, callback);
  };
  
module.exports = {
    passengerMovement,
    exportMethods
}