'use strict'
//Import Required Models
const passengerMovementService = require('./passengerMovementService');
const response = require('../../../services/responseService');

/**
 * add continous movements to the database
 */
module.exports.saveNewPassengerMovement = function (req, res) {
    passengerMovementService.newPassengerMovement(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}