'use strict'
//Imports
const passengerMovementModel = require('./passengerMovementModel').passengerMovement;
const passengerMovementMethods = require('./passengerMovementModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');


/**
 * add new confirmation to the system
 */

module.exports.newPassengerMovement = function (body, res, callBack) {
    const passengerMovement = new passengerMovementModel();
    passengerMovement.status = body.status;
    passengerMovementMethods.savePassengerMovement(passengerMovement, function (err) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('movement saved'));
        }
    });
}