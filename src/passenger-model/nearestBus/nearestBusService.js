'use strict'
//Imports

const nearestBusstopService = require('../../passenger-model/NearestBusstop/NearestBusstopService');
const passengerLocationDataService = require('../../passenger-model/passengerLocationData/passengerLocationDataService');
const busTrackingModelMethods = require('../../driver-model/busTracking/trackingModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const activeStatus = "Active";

module.exports.getNearestBus = function (body, res, callBack) {
    nearestBusstopService.getNearestBusstop({ passenger_id: body.passenger_id, route_no: body.route_no }, {}, function (data, err) {
        console.log(data);
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (data != undefined || data != null || data.length != 0) {
            var _passenger = data.data;

           
            busTrackingModelMethods.findRoute({route_no : body.route_no, status : activeStatus }, 'bus longitude latitude',function (err, tracker){
                // console.log(tracker != undefined && tracker != null && tracker.length != 0);
                console.log(tracker);
            if(err){
                
                callBack(callBackResponse.callbackWithDefaultError());
            }else if(tracker != undefined && tracker != null && tracker.length != 0){

                console.log("v");
                var _min = {};

                tracker.forEach((_bus, index) => {
                    
                    var passenger_latitude = Math.PI * _passenger.lat / 180;
                    var bus_latitude = Math.PI * _bus.latitude / 180;
                    var theta = Math.PI * (_passenger.lng - _bus.longitude) / 180;


                    var dist = Math.sin(passenger_latitude) * Math.sin(bus_latitude) + Math.cos(passenger_latitude) * Math.cos(bus_latitude) * Math.cos(theta);
                    if (dist > 1) {
                        dist = 1;
                    }

                    dist = (((Math.acos(dist) * 180 / Math.PI) * 60 * 1.1515) * 1.609344) * 1000;
                        if (index == 0) {
                            _min.dist = dist;
                            _min.bus = _bus;
                        } else {
                            if (dist < _min.dist) {
                                _min.dist = dist;
                                _min.bus = _bus;
                            }
                        }

                });

                callBack(callBackResponse.callbackWithData(_min.bus));
                //console.log(_min.bus);

            }else{
                callBack(callBackResponse.callbackWithfalseMessage('No nearest bus found'));
            }

            });
            // callBack(callBackResponse.callbackWithData(data));
        }
        else {
            callBack(callBackResponse.callbackWithfalseMessage('No nerest bus found'));
        }

    });



}

