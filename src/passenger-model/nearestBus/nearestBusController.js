'use strict'
//Import Required Models
const nearestBusService = require('./nearestBusService');
const response = require('../../../services/responseService');

/**
 * get nearest bus stop
 */
module.exports.nearestBus = function (req, res) {
    nearestBusService.getNearestBus(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}