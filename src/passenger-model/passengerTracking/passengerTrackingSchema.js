//import validator class
const joi = require('joi');

//newPassengerTracker Route Schema and validations to be done 
module.exports.newPassengerTracker = joi.object().keys({
    longitude: joi.string().required(),
    latitude: joi.number().required(),
    passenger_id: joi.string().alphanum().min(24).max(24).required()
});

//update Route Schema and validations to be done 
module.exports.updatePassengerTracker = joi.object().keys({
    passengerTracker_id: joi.string().alphanum().min(24).max(24).required(),
    longitude: joi.string(),
    latitude: joi.number(),
    passenger_id: joi.string().alphanum().min(24).max(24).required()
});

//passengerTracker_id Schema and validations to be done 
module.exports.trackerID = joi.object().keys({
    passengerTracker_id: joi.string().alphanum().min(24).max(24).required()
});