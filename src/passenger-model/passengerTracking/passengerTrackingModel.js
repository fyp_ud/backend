const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};


const passengerTrackingSchema = new Schema({
    longitude : String,
    latitude : String,
    passenger_id : { type: mongoose.Schema.Types.ObjectId, ref: 'Passenger' },
    status : String
}, { timestamps: true });

var passengerTracker = mongoose.model('passengerTracker', passengerTrackingSchema);

exportMethods.savepassengerTracker = (newPassengerTracker, callback) => {
    newPassengerTracker.save(callback);
}

exportMethods.findOne = (query, options, callback) => {
    passengerTracker.findOne(query, options, callback);
}

exportMethods.find = (query, options, callback) => {
    passengerTracker.find(query, options, callback);
};
  
exportMethods.findByID = (query, options, callback) => {
    passengerTracker.findById(query, options, callback);
};
  
exportMethods.fincByIdandUpdate = (query, trackerInfo, options, callback) => {
    passengerTracker.findByIdAndUpdate(query, trackerInfo, options, callback);
}

exportMethods.findOneAndUpdate=(query,trackerInfo, options, callback)=>{
    passengerTracker.findOneAndUpdate(query,trackerInfo, options, callback);
}

module.exports = {
    exportMethods,
    passengerTracker
}