'use strict'

const passengerTrackingService = require('./passengerTrackingService');
const response = require('../../../services/responseService');

/**
 * add start location to the database
 */
module.exports.newPassengerTracking = function (req, res) {
    passengerTrackingService.startPassengerTracking(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}


/**
 * update the location of the tracker
 */
module.exports.updatePassengerTracker = function (req, res) {
    passengerTrackingService.passengerTrackingUpdate(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * remove the tracker
 */
module.exports.removePassengerTracker = function (req, res) {
    passengerTrackingService.removeTracker(req.body, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}