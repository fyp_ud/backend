'use strict'
//Imports
const paseengerTrackingModel = require('./passengerTrackingModel').passengerTracker;
const passengerTrackerModelMethods = require('./passengerTrackingModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactive";

/**
 * create new passenger tracker
 */
module.exports.startPassengerTracking = function (body, res, callBack) {
    const passengerTracker = new paseengerTrackingModel();
    passengerTracker.longitude = body.longitude;
    passengerTracker.latitude = body.latitude;
    passengerTracker.passenger_id = body.passenger_id;
    passengerTracker.status = activeStatus;
    passengerTrackerModelMethods.savepassengerTracker(passengerTracker, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}


/**
 * update passenger tracker
 */
module.exports.passengerTrackingUpdate = function (body, res, callBack) {
    var object = JSON.parse(JSON.stringify(body))
    delete object['passengerTracker_id']
    passengerTrackerModelMethods.fincByIdandUpdate({ _id: body.passengerTracker_id }, object, { "new": true }, function (err, track) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (track == null || track == undefined || track.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage(track));
        }
    });
}

//remove Tracker
module.exports.removeTracker = function (body, res, callBack) {
    passengerTrackerModelMethods.findOneAndUpdate({passenger_id:body.id,status:activeStatus}, { status: deactiveStatus }, { safe: true }, function (err, passenger) {

        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (passenger == null || passenger == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Tracker Not Found'));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('Tracker Removed From The System'));
        }
    });
};

module.exports.getActiveTracker = function (body, res, callBack) {
    passengerTrackerModelMethods.findOne({passenger_id : body.id,status:activeStatus},'_id',function(err,tracker){
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (tracker != undefined || tracker != null) {
            callBack(callBackResponse.callbackWithData(tracker));
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('no coordinates found'));
        }
    });
}
