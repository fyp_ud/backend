'use strict'
//Imports

const busRouteModelMethods = require('../../driver-model/busRoute/busRouteModel').exportMethods;
const passengerLocationModelMethods = require('../../passenger-model/passengerLocationData/passengerLocationDataModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');

module.exports.getNearestBusstop = function( body, res, callBack){
    passengerLocationModelMethods.find({passenger_id : body.passenger_id}, 'longitude latitude',function(err,passengercoordinate){
    
        if(err){
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (passengercoordinate != undefined || passengercoordinate != null || passengercoordinate.length != 0) {
            var _passenger = passengercoordinate[passengercoordinate.length-1];
        //    console.log('#passenger --------> '+JSON.stringify(_passenger));

            busRouteModelMethods.find({route_no : body.route_no} , 'bus_stops',function(err, busStops){
                
                if(err){
                    callBack(callBackResponse.callbackWithDefaultError());
                }else if(busStops != undefined || busStops != null || busStops.length != 0){
                    var busstopCoordinates = busStops[0].bus_stops;

                    var _min = {};

                    busstopCoordinates.forEach((_busstop , index) => {
                       
                        var passenger_laitude = Math.PI * _passenger.latitude / 180;
                        var busstop_latitude = Math.PI * _busstop.lat / 180;
                        var theta = Math.PI * ((_passenger.longitude - _busstop.lng) / 180);

                        var dist = Math.sin(passenger_laitude) * Math.sin(busstop_latitude) + Math.cos(passenger_laitude) * Math.cos(busstop_latitude) * Math.cos(theta);
                        
                        if (dist > 1) {
                            dist = 1;
                        }
                        
                        dist = (((Math.acos(dist) * 180 / Math.PI) * 60 * 1.1515) * 1.609344) * 1000 ;
                        // console.log('#dist------> '+dist)
                        if (index == 0) {
                            _min.dist = dist;
                            _min.busstop = _busstop;
                        } else {
                            if (dist < _min.dist) {
                                _min.dist = dist;
                                _min.busstop = _busstop;
                            }
                        }
                        
                    });
                       console.log(_min.busstop);
                     callBack(callBackResponse.callbackWithData(_min.busstop));

                }
                else{
                    callBack(callBackResponse.callbackWithfalseMessage('no nearest bus found'));
                }
                
                
            })
           
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('no coordinates found'));
        }
        
    });

}