'use strict'
//Import Required Models
const nearestBusstopService = require('./NearestBusstopService');
const response = require('../../../services/responseService');

/**
 * get nearest bus stop
 */
module.exports.NearestBusstop = function (req, res) {
    nearestBusstopService.getNearestBusstop(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}