const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const exportMethods = {};

const behaviourSchema = new Schema({

passenger_id:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Passenger'
},
route_no : {
    type: String,
    required : true
},
timeSlot : {
    type: String,
    required : true
},
busStartLocation:{
    type: String,
    // required : true
}

}, { timestamps: true });

var behaviors = mongoose.model('passengerBehaviors', behaviourSchema);

exportMethods.savePassengerBehavior = (newBehaviors, callback) => {
    newBehaviors.save(callback);
  }
  
exportMethods.findOne = (query, options, callback) => {
    behaviors.findOne(query, options, callback);
  }
  
exportMethods.find = (query, options, callback) => {
    behaviors.find(query, options, callback);
  };
  
exportMethods.findByID = (query, options, callback) => {
    behaviors.findById(query, options, callback);
  };
  
exportMethods.findByIdandUpdate = (query, behavior, options, callback) => {
    behaviors.findByIdAndUpdate(query, behavior, options, callback);
  };
  
module.exports = {
    behaviors,
    exportMethods
}
