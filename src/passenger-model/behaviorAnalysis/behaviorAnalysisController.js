'use strict'
//Import Required Models
const behaviorAnalysisService = require('./behaviorAnalysisService');
const response = require('../../../services/responseService');

/**
 * add continous record to the data base
 */
module.exports.saveSearchRecord = function (req, res) {
    behaviorAnalysisService.newSearchRecord(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * update search records
 */
module.exports.updateSearchRecord = function (req, res) {
    behaviorAnalysisService.passengerSearchDataUpdate(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * find behaviors
 */
module.exports.newBehavior = function (req, res) {
    behaviorAnalysisService.findBehaviors(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}


/*
* 
*/
module.exports.getAll = function (req, res) {
    behaviorAnalysisService.getAllbehaviors(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}