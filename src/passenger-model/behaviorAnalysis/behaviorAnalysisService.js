'use strict'
//Imports
const searchHistoryModel = require('./searchHistoryModel').passengerSearchHistory;
const searchHistoryMethods = require('./searchHistoryModel').exportMethods;
const behaviorAnalysisModel = require('./behaviorAnalysisModel').behaviors;
const behaviorAnalysisMethods = require('./behaviorAnalysisModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');
const generateSchedule = require('../../../config/timeSlots');
const activeTracker = require('../../driver-model/busTracking/trackingService');
var noSearch = 1;
const mongoose = require('mongoose');
const moment = require('moment');
var format = 'hh:mm:ss';
var time_slot = "";

const curDate = new Date().toString();
const currentDate = moment().format("YYYY-MM-DD");
const currentTime = moment(curDate.split(" ")[4], format);


/**
 * add new serch record to the system
 */

module.exports.newSearchRecord = function (body, res, callBack) {



    const searchRecord = new searchHistoryModel();
    searchRecord.passenger_id = body.passenger_id;
    searchRecord.current_location = body.current_location;
    searchRecord.destination = body.destination;
    searchRecord.date = currentDate;
    searchRecord.time = currentTime;
    searchRecord.NoofSearch = noSearch;
    searchRecord.busStartLocation = body.busStartLocation;
    searchRecord.route_no = body.route_no;


    //find time slot
    var rs = generateSchedule.timeAllocations.paramters.filter(_schedule => {
        var beforeTime = moment(_schedule.startTime, format);
        var afterTime = moment(_schedule.endTime, format);
        return currentTime.isBetween(beforeTime, afterTime);
    });

    time_slot = ((rs[0].startTime).concat('-')).concat(rs[0].endTime);

    searchRecord.timeSlot = time_slot;

    searchHistoryMethods.savePassengerSearchRecord(searchRecord, function (err) {
        if (err) {
            console.log(err);
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('record saved'));
        }
    });
}

/**
 * update search record
 */
module.exports.passengerSearchDataUpdate = function (body, res, callBack) {

    //find the suitable timeslot


    var rs = generateSchedule.timeAllocations.paramters.filter(_schedule => {
        console.log(_schedule);
        var beforeTime = moment(_schedule.startTime, format);
        var afterTime = moment(_schedule.endTime, format);
        return currentTime.isBetween(beforeTime, afterTime);
    });
    if (rs == null || rs == undefined || rs.length == 0) {
        callBack(callBackResponse.callbackWithfalseMessage("No buses in this time"));
    } else {
        time_slot = ((rs[0].startTime).concat('-')).concat(rs[0].endTime);

        activeTracker.getTrackerByBusID(body.bus_id, {}, function (tracker, err) {
            console.log("-----> tracker")
            // console.log(tracker);
            if (tracker.status) {

                delete body['bus_id'];
                body['busStartLocation'] = tracker.data.start_location;

                searchHistoryMethods.find({ passenger_id: mongoose.Types.ObjectId(body.passenger_id), current_location: body.current_location, destination: body.destination, timeSlot: time_slot, busStartLocation: body.busStartLocation, route_no: body.route_no }, {}, function (err, details) {

                    if (err) {
                        callBack(callBackResponse.callbackWithfalseMessage(err));
                    }
                    else if (details == null || details == undefined || details.length == 0) {
                        // callBack(callBackResponse.callbackWithfalseMessage("New record found"));
                        const searchRecord = new searchHistoryModel();
                        searchRecord.passenger_id = body.passenger_id;
                        searchRecord.current_location = body.current_location;
                        searchRecord.destination = body.destination;
                        searchRecord.date = currentDate;
                        searchRecord.time = currentTime;
                        searchRecord.NoofSearch = noSearch;
                        searchRecord.busStartLocation = body.busStartLocation;
                        searchRecord.route_no = body.route_no;
                        searchRecord.timeSlot = time_slot;
                        searchHistoryMethods.savePassengerSearchRecord(searchRecord, function (err) {
                            if (err) {
                                console.log(err);
                                callBack(callBackResponse.callbackWithDefaultError());
                            } else {
                                callBack(callBackResponse.callbackWithSucessMessage('record saved'));
                            }
                        });
                    }
                    else {

                        searchHistoryMethods.findByIdandUpdate({ _id: details[0]._id }, { passenger_id: mongoose.Types.ObjectId(body.passenger_id), current_location: body.current_location, destination: body.destination, time: currentTime, timeSlot: time_slot, busStartLocation: body.busStartLocation, route_no: body.route_no, date: currentDate, $inc: { NoofSearch: 1 } }, { "new": true }, function (err, record) {
                            if (err) {
                                callBack(callBackResponse.callbackWithfalseMessage(err));
                            } else if (record == null || record == undefined || record.length == 0) {
                                callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
                            } else {
                                if (record.NoofSearch > 10) {
                                    behaviorAnalysisMethods.find({ route_no: record.route_no, busStartLocation: record.busStartLocation, timeSlot: record.timeSlot, passenger_id: mongoose.Types.ObjectId(record.passenger_id) }, {}, function (err, newRecord) {

                                        if (err) {
                                            callBack(callBackResponse.callbackWithfalseMessage(err));
                                        }
                                        else if (newRecord[0] == null || newRecord[0] == undefined || newRecord[0].length == 0) {
                                            // console.log('data------>' + index);
                                            console.log(record)

                                            const newBehavior = new behaviorAnalysisModel();
                                            newBehavior.passenger_id = mongoose.Types.ObjectId(record.passenger_id);
                                            newBehavior.timeSlot = record.timeSlot;
                                            newBehavior.busStartLocation = record.busStartLocation;
                                            newBehavior.route_no = record.route_no;
                                            behaviorAnalysisMethods.savePassengerBehavior(newBehavior, function (err) {
                                                if (err) {
                                                    callBack(callBackResponse.callbackWithDefaultError());
                                                }
                                            });
                                        }

                                    });
                                }
                                callBack(callBackResponse.callbackWithData(record));
                            }
                        });

                    }
                })
            }
            else{
                callBack(callBackResponse.callbackWithfalseMessage("Active tracker not found"));
            }

        })

    }





}

/**
 * get all
 */

module.exports.getAllbehaviors = function (body, res, callBack) {
    behaviorAnalysisMethods.find({}, {}, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        }
        else if (data != null && data != undefined && data.length != 0) {
            callBack(callBackResponse.callbackWithData(data));
        }
        else {
            callBack(callBackResponse.callbackWithDefaultError('behaviors not found'));
        }
    })
}