const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const exportMethods = {};

const searchHistorySchema = new Schema({

    passenger_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Passenger'
    },
    current_location:{
        type: String,
        required : true
    },
    destination:{
        type: String,
        required : true
    },
    date :{ 
        type : Date,
        required : true
    },
    time: {
        type: String,
        required : true
    },
    timeSlot : {
        type: String,
        required : true
    },
    NoofSearch : Number,
    busStartLocation : {
        type: String,
        // required : true
    },
    route_no : {
        type: String,
        required : true
    }
}, { timestamps: true });

var passengerSearchHistory = mongoose.model('searchHistory', searchHistorySchema);

exportMethods.savePassengerSearchRecord = (newSearchRecord, callback) => {
    newSearchRecord.save(callback);
  }
  
exportMethods.findOne = (query, options, callback) => {
    passengerSearchHistory.findOne(query, options, callback);
  }
  
exportMethods.find = (query, options, callback) => {
    passengerSearchHistory.find(query, options, callback);
  };
  
exportMethods.findByID = (query, options, callback) => {
    passengerSearchHistory.findById(query, options, callback);
  };
  
exportMethods.findByIdandUpdate = (query, passengerSearchRecord, options, callback) => {
    passengerSearchHistory.findByIdAndUpdate(query, passengerSearchRecord, options, callback);
  };
  
module.exports = {
    passengerSearchHistory,
    exportMethods
}