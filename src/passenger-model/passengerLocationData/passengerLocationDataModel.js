const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const exportMethods = {};

const passengerLocationDataSchema = new Schema({
    longitude: {
        type: String,
        required: true,
    },
    latitude: {
        type: String,
        required: true
    },
    passenger_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Passenger'
    },
    passengerTracker_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'passengerTracker'
    }
}, { timestamps: true });

var passengercoordinate = mongoose.model('passengerCoordinates', passengerLocationDataSchema);

exportMethods.savePassengerCoordinates = (newPassegerCoordinate, callback) => {
    newPassegerCoordinate.save(callback);
  }
  
exportMethods.findOne = (query, options, callback) => {
    passengercoordinate.findOne(query, options, callback);
  }
  
exportMethods.find = (query, options, callback) => {
    passengercoordinate.find(query, options, callback);
  };
  
exportMethods.findByID = (query, options, callback) => {
    passengercoordinate.findById(query, options, callback);
  };
  
exportMethods.fincByIdandUpdate = (query, passengerCoordinate, options, callback) => {
    passengercoordinate.findByIdAndUpdate(query, passengerCoordinate, options, callback);
  };
  
module.exports = {
    passengercoordinate,
    exportMethods
}
