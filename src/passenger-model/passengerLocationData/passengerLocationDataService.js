'use strict'
//Imports
const passengerLocationDataModel = require('./passengerLocationDataModel').passengercoordinate;
const passengerLocationDataMethods = require('./passengerLocationDataModel').exportMethods;
const callBackResponse = require('../../../services/callBackResponseService');

/**
 * add new cordinates to the system
 */

module.exports.newPassengerCordinate = function (body, res, callBack) {
    const passengerCoordinate = new passengerLocationDataModel();
    passengerCoordinate.longitude = body.longitude;
    passengerCoordinate.latitude = body.latitude;
    passengerCoordinate.passengerTracker_id = body.passengerTracker_id;
    passengerCoordinate.passenger_id = body.passenger_id;
    passengerLocationDataMethods.savePassengerCoordinates(passengerCoordinate, function (err) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('coordinate saved'));
        }
    });
}

/**
 * get raw cordinates according to the data
 */
module.exports.getPassengerRawCoordinates = function (body, res, callBack) {
    passengerLocationDataMethods.find({ passenger_id: body.passenger_id }, 'longitude , latitude', function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (data != undefined || data != null || data.length != 0) {
            callBack(callBackResponse.callbackWithData(data));
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('no coordinates found'));
        }
    });
}

//,updatedAt: { $lte: body.startDate } 
