'use strict'
//Import Required Models
const passengerLocationDataService = require('./passengerLocationDataService');
const response = require('../../../services/responseService');

/**
 * add continous locations to the database
 */
module.exports.saveNewPassengerCoordinate = function (req, res) {
    passengerLocationDataService.newPassengerCordinate(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get data according the passenger_id and the time stamp
 */
module.exports.getNewRawCoordinates = function (req, res) {
    passengerLocationDataService.getPassengerRawCoordinates(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}