'use strict'
//Imports

const nearestBusstopService = require('../../passenger-model/NearestBusstop/NearestBusstopService');
const nearestBusService = require('../../passenger-model/nearestBus/nearestBusService');
const callBackResponse = require('../../../services/callBackResponseService');
const config = require('../../../config/config');

var activeStatus = "Active"
const request = require('request');
const moment = require('moment');
var _eta = 0;

module.exports.calculateDistance = function (body, res, callBack) {
    nearestBusstopService.getNearestBusstop({ passenger_id: body.passenger_id, route_no: body.route_no }, {}, function (data, err) {
        var _passenger = data.data;

        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage('No nearest bus found'));
        }
        else {

            nearestBusService.getNearestBus({ passenger_id: body.passenger_id, route_no: body.route_no, status: activeStatus }, {}, function (nearestBus, err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                }
                else if (nearestBus == null || nearestBus == undefined || nearestBus.length == 0) {
                    callBack(callBackResponse.callbackWithfalseMessage('No nearest bus found'));
                }
                else {
                    var _bus = nearestBus.data;

                    //calculated distance
                    var _dist = calDistance(_passenger.lat, _passenger.lng, _bus.latitude, _bus.longitude);
                    console.log(_dist);

                    //weather details
                    weatherDetails(_bus.latitude, _bus.longitude, function (weatherData, err) {

                        if (err) {
                            callBack(callBackResponse.callbackWithDefaultError());
                        }
                        else if (weatherData == null || weatherData == undefined || weatherData.length == 0) {
                            callBack(callBackResponse.callbackWithfalseMessage('No nearest bus found'));
                        }
                        else {
                            weatherData = JSON.parse(weatherData);

                            let _temp = weatherData['current']['temp_c'] + 273.15;
                            let _press = weatherData['current']['pressure_mb'] * 100;
                            let _windSpeed = ((weatherData['current']['wind_kph'] * 3600) / 1000);
                            let _windDirection = weatherData['current']['wind_dir'];

                            //eta prediction
                            etaAPI(_dist, _temp, _press, _windSpeed, _windDirection, _eta, function (prediction, err) {

                                if (err) {
                                    callBack(callBackResponse.callbackWithDefaultError());
                                }
                                else if (prediction == null || prediction == undefined || prediction.length == 0) {
                                    callBack(callBackResponse.callbackWithfalseMessage('Predicted eta found'));
                                }
                                else {
                                    _eta = parseFloat(prediction['output']);
                                    console.log(_eta);
                                    callBack(callBackResponse.callbackWithData(_eta));
                                }
                            });
                        }

                    });
                }
            });

        }
    });
}

//get weather detail through api
function weatherDetails(lat, lng, callBack) {
    var options = {
        url: config.weatherAPI + lat + ',' + lng,
        headers: {
            'User-Agent': 'request',
            'Content-Type': 'application/json; charset=utf-8'
        }
    };
    request.post(options, function (err, res, body) {
        if (!err && res.statusCode == 200) {
            callBack(body);
        }
    });
}

//calculate distance
function calDistance(_passengerlatitude, _passengerlongitude, _buslatitude, _buslongitude) {

    var passenger_latitude = Math.PI * _passengerlatitude / 180;
    var bus_latitude = Math.PI * _buslatitude / 180;
    var theta = Math.PI * (_passengerlongitude - _buslongitude) / 180;

    var dist = Math.sin(passenger_latitude) * Math.sin(bus_latitude) + Math.cos(passenger_latitude) * Math.cos(bus_latitude) * Math.cos(theta);
    if (dist > 1) {
        dist = 1;
    }
    dist = (((Math.acos(dist) * 180 / Math.PI) * 60 * 1.1515) * 1.609344) * 1000;
    //callBack(dist);
    return dist;

}


//eta prediction API
function etaAPI(_dist, temp, press, iws, cbwd, eta, callBack) {
    var options = {
        url: config.etaPredictionAPI,
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: {
            date: { "y": moment().year(), "M": moment().month() + 1, "d": moment().date(), "h": moment().hour(), "m": moment().minute() },
            eta: eta,
            PRES: press,
            Iws: iws,
            traffic: 0,
            distance: _dist,
            TEMP: temp,
            cbwd: cbwd,
            Ir: 0
        },
        json: true
    };

    request.post(options, function (err, res, body) {
        if (!err && res.statusCode == 200) {
            callBack(body);
        }
    });
}