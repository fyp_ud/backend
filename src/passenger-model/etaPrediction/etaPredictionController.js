'use strict'
//Import Required Models
const etaPredictionService = require('./etaPredictionService');
const response = require('../../../services/responseService');

/**
 * get nearest bus stop
 */
module.exports.getEta = function (req, res) {
    etaPredictionService.calculateDistance(req.body, res, function (data) {
        if (data.status) {
            return response.successWithMessage(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}