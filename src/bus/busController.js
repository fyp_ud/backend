'use strict'

//Import Required modules
const busService = require('./busService');
const response = require('../../services/responseService');
var exportMethods = {};

/**
 * add new bus to the system
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.newBus = function (req, res) {
    busService.saveBus(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * edit bus from the system
 * @param {*} req  
 * @param {*} res 
 */
exportMethods.updateBus = function (req, res) {
    busService.updateBus(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get all buses from the system
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getBuses = function (req, res) {
    busService.getBuses(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * remove bus from the database
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.removeBus = function (req, res) {
    busService.removeBus(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get bus from bus no
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.findFromBusNo = function (req, res) {
    busService.findBusFromBusNo(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get bus from bus no
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.findBusByRoute = function (req, res) {
    busService.findBusByRoute(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
* find bus from bus_id
* @param {*} req 
* @param {*} res 
*/
exportMethods.findFromID = function (req, res) {
    busService.findFromBusID(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get buses according to the route
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getActiveBusesFromRoute = function (req, res) {
    busService.getActiveBusesFromRoute(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}

/**
 * get buses according to the route
 * @param {*} req 
 * @param {*} res 
 */
exportMethods.getActiveBusesByLocation = function (req, res) {
    busService.getActiveBusesByLocation(req, res, function (data) {
        if (data.status) {
            return response.successWithData(data.data, res)
        } else {
            return response.customError(data.data, res);
        }
    });
}


module.exports = exportMethods;