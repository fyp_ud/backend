'use strict'
//Imports
const busModelMethods = require('./busModel').exportMethods;
const busModel = require('./busModel').busModel;
const callBackResponse = require('../../services/callBackResponseService');
const activeStatus = "Active";
const deactiveStatus = "Deactivate";
var exportModel = {};

/**
 * get all buses from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.getBuses = (req, res, callBack) => {
    busModelMethods.find({ busStatus: { $ne: deactiveStatus } }, function (err, busObj) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined || busObj.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No bus found"));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    });
}

/**
 * find buses from bus No
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.findBusFromBusNo = (req, res, callBack) => {
    busModelMethods.findOne({ busNo: req.body.bus_no }, (err, busObj) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

/**
 * save bus from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.saveBus = (req, res, callBack) => {
    const newBus = new busModel({
        busNo: req.body.busNo,
        busType: req.body.busType,
        owner: req.body.owner,
        routeNo: req.body.routeNo,
        ownerContactNo: req.body.ownerContactNo,
        noOfSeats: req.body.noOfSeats,
        driverName: req.body.driverName,
        driverContact: req.body.driverContact,
        onGoingStatus: deactiveStatus,
        busStatus: activeStatus
    });
    busModelMethods.saveBus(newBus, (err, bus) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(bus));
        }
    })
}

/**
 * update bus from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBackResponse 
 */
exportModel.updateBus = (req, res, callBack) => {
    var bus = JSON.parse(JSON.stringify(req.body))
    delete bus['bus_id']

    busModelMethods.fincByIdandUpdate(req.body.bus_id ? req.body.bus_id : req.body._id, bus, { new: true, safe: true }, function (err, busObj) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage("No bus found"));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

/**
 * get bus status from the database
 * @param {*} req 
 * @param {*} res 
 * @param {*} callBackResponse 
 */
exportModel.removeBus = (req, res, callBack) => {
    busModelMethods.fincByIdandUpdate(req.body.bus_id, { busStatus: deactiveStatus }, { new: true, safe: true }, function (err, busObj) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage("No bus found"));
        } else {
            callBack(callBackResponse.callbackWithSucessMessage("Bus removed from the system"));
        }
    })
}

/**
 * find buses from bus No
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBack 
 */
exportModel.findBusByRoute = (req, res, callBack) => {
    busModelMethods.find({ routeNo: req.body.routeNo, busStatus: activeStatus }, (err, busObj) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

/*
* find bus from bus_id
* @param {*} req 
* @param {*} res 
* @param {*} callBack 
*/
exportModel.findFromBusID = (req, res, callBack) => {
    busModelMethods.findByID(req.body.bus_id, (err, busObj) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

/*
* find active buses from ongoing status according to the route
* @param {*} req 
* @param {*} res 
* @param {*} callBack 
*/
exportModel.getActiveBusesFromRoute = (req, res, callBack) => {
    busModelMethods.find({ routeNo: req.body.route_id, onGoingStatus: req.body.onGoingStatus, busStatus: activeStatus }, (err, busObj) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined || busObj.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No Buses Found"));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

exportModel.getActiveBusesByLocation = (req, res, callBack) => {
    busModelMethods.find({ routeNo: req.body.route_id, onGoingStatus: req.body.onGoingStatus, busStatus: activeStatus, start_location:req.body.start_location }, (err, busObj) => {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined || busObj.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No Buses Found"));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}






/**
 * update bus from the database
 * @param {*} res 
 * @param {*} res 
 * @param {*} callBackResponse 
 */
exportModel.bulkUpdateBus = (req, res, callBack) => {

    busModelMethods.bulkUpdate({ busStatus: activeStatus }, { totalRevenue: 0 }, { new: true, safe: true }, function (err, busObj) {
        if (err) {
            callBack(callBackResponse.callbackWithfalseMessage(err));
        } else if (busObj == null || busObj == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage("No bus found"));
        } else {
            callBack(callBackResponse.callbackWithData(busObj));
        }
    })
}

module.exports = exportModel;