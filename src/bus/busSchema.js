//import validator class
const joi = require('joi');

//new bus Schema and validations to be done 
module.exports.newBus = joi.object().keys({
    busNo: joi.string().min(6).required(),
    busType: joi.string().min(1).required(),
    owner: joi.string().min(2).required(),
    ownerContactNo: joi.number().required(),
    noOfSeats: joi.number().required(),
    routeNo: joi.string().alphanum().min(24).max(24).required(),
    driverName: joi.string().min(2).required(),
    driverContact: joi.number().required(),
    totalRevenue: joi.number().required(),
});

//updateBus Route Schema and validations to be done 
module.exports.updateBus = joi.object().keys({
    bus_id: joi.string().alphanum().min(24).max(24).required(),
    busNo: joi.string().min(6),
    busType: joi.string().min(1),
    owner: joi.string().min(2),
    ownerContactNo: joi.number(),
    noOfSeats: joi.number(),
    routeNo: joi.string(),
    driverName: joi.string().min(2),
    driverContact: joi.number(),
    noOfTurns: joi.number(),
    totalRevenue: joi.number(),
    onGoingStatus: joi.string()
});

//busID Schema and validations to be done 
module.exports.busID = joi.object().keys({
    bus_id: joi.string().alphanum().min(24).max(24).required(),
});

//route_id Schema and validations to be done 
module.exports.busNo = joi.object().keys({
    bus_no: joi.string().min(6).required(),
});

//activeBus Schema and validations to be done 
module.exports.activeBus = joi.object().keys({
    route_id: joi.string().alphanum().min(24).max(24).required(),
    onGoingStatus: joi.string().required(),
});

//activeBus Schema and validations to be done for location
module.exports.activeBusByLocation = joi.object().keys({
    route_id: joi.string().alphanum().min(24).max(24).required(),
    start_location: joi.string().alphanum().required(),
    onGoingStatus: joi.string().required(),
});