'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exportMethods = {};

const busSchema = new Schema({

    busNo: {
        type: String,
        required: true
    },
    busType: {
        type: String,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    ownerContactNo: {
        type: Number,
        required: true
    },
    noOfSeats: {
        type: Number,
        required: true
    },
    routeNo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'busRoute',
        required: true
    },
    driverName: {
        type: String,
        required: true
    },
    driverContact: {
        type: Number,
        required: true
    },
    busStatus: {
        type: String,
        required: true
    },
    noOfTurns: {
        type: Number
    },
    totalRevenue: {
        type: Number
    },
    onGoingStatus: {
        type: String
    }

}, { timestamps: true });

var busModel = mongoose.model("Bus", busSchema);

exportMethods.saveBus = (newBus, callback) => {
    newBus.save(callback);
}

exportMethods.findOne = (query, callback) => {
    busModel.findOne(query).populate('routeNo').lean().exec(callback);
}

exportMethods.find = (query, callback) => {
    busModel.find(query).populate('routeNo').lean().exec(callback);
};

exportMethods.findByID = (query, callback) => {
    busModel.findById(query).populate('routeNo').lean().exec(callback);
};

exportMethods.fincByIdandUpdate = (query, busInfo, options, callback) => {
    busModel.findByIdAndUpdate(query, busInfo, options, callback);
}

exportMethods.bulkUpdate = (query, busInfo, options, callback) => {
    busModel.update(query, busInfo, options, callback);
}

module.exports = {
    exportMethods,
    busModel
}