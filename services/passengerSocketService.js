//import required libraries
const socketio = require('socket.io');
const passengerTrackerService = require('../src/passenger-model/passengerTracking/passengerTrackingService');
const passengerLocationService = require('../src/passenger-model/passengerLocationData/passengerLocationDataService');

let io;
let connectedUsers = [];

//initialize the socket
function listen(app) {
    io = socketio.listen(app);
    io.on('connection', socket => {
        console.log('#Socket --------> user connected');

        socket.on('start-track', function (location) {
            console.log('#track ------> Location');
            passengerTrackerService.getActiveTracker({ id: location.passenger_id }, "res", function (activeTracker) {
                if (activeTracker.status) {
                    location['passengerTracker_id'] = activeTracker.data._id;
                    passengerTrackerService.passengerTrackingUpdate(location, "res", function (data) {
                        console.log('#log --------> update Tracker');
                        passengerLocationService.newPassengerCordinate(location, "res", function (data) {
                            console.log('#log --------> Save Cordinate');
                        });
                    });
                } else {
                    passengerTrackerService.startPassengerTracking(location, "res", function (data) {
                        console.log('#log --------> start Tracker');
                        passengerLocationService.newPassengerCordinate(location, "res", function (data) {
                            console.log('#log --------> Save Cordinate');
                        });
                    });
                }
            });
        });

        socket.on('stop-track', function (user) {
            passengerTrackerService.removeTracker(user,'res',function(data){
                console.log('#stop ------> track');
                console.log(data);
            });
        });
        // socket.on('create', function (user) {
        //     console.log(user);
        // });

        // socket.on('disconnect', function () {
        //     console.log('user disconnected');
        //     var count = 0
        //     connectedUsers.forEach(element => {
        //         if (element.socket_id == socket.id) {
        //             connectedUsers.splice(count, 1);
        //         }
        //         count++;
        //     });
        //     console.log(connectedUsers)
        // });

        // socket.on('location', (locationObj) => {
        //     console.log(locationObj)
        //     var obj = {};
        //     obj.status = "Active";
        //     obj.bus_id = locationObj.bus;
        //     trackingService.checkActiveTracker(obj, "res", (data) => {
        //         console.log(data)
        //         if (data.status) {
        //             if (locationObj.status == "Deactive") {
        //                 journeyService.journeyEnd({
        //                     bus_id: locationObj.bus,
        //                     tracker: data.data._id,
        //                     endTime: locationObj.endTime,
        //                     total_KM: locationObj.total_km
        //                 }, "", (journey) => {
        //                     var location = {};
        //                     location.longitude = data.data.longitude;
        //                     location.latitude = data.data.latitude;
        //                     location.status = "Deactive";
        //                     location.tracker_id = data.data._id;
        //                     busStatus({ bus_id: locationObj.bus, status: "Deactivate" })
        //                     tracker(location);
        //                     location.tracker_id = data.data._id;
        //                     location.bus = locationObj.bus;
        //                     mineLocationData(location);
        //                 })
        //             } else {
        //                 var location = {};
        //                 location.longitude = locationObj.longitude;
        //                 location.latitude = locationObj.latitude;
        //                 location.status = "Active";
        //                 location.tracker_id = data.data._id;
        //                 tracker(location);
        //                 location.bus = locationObj.bus;
        //                 mineLocationData(location);
        //             }
        //         } else {
        //             var location = {};
        //             location.longitude = locationObj.longitude;
        //             location.latitude = locationObj.latitude;
        //             location.status = "Active";
        //             location.bus = locationObj.bus;
        //             location.route_id = locationObj.route_id;
        //             location.route_no = "157";
        //             location.start_location = locationObj.start_location;
        //             trackingService.startTracking(location, " ", (tracker) => {
        //                 if (tracker.status) {
        //                     var journey = {};
        //                     journey.startTime = locationObj.startTime
        //                     journey.bus = locationObj.bus;
        //                     journey.tracker = tracker.data._id;
        //                     journeyService.startJourney(journey, "", (journeyStatus) => {
        //                         if (journeyStatus.status) {
        //                             location.tracker_id = tracker.data._id
        //                             mineLocationData(location);
        //                             busStatus({ bus_id: locationObj.bus, status: "Active" });
        //                             emitActiveTrackers();
        //                         } else {
        //                             emitActiveTrackers()
        //                             console.log(journeyStatus);
        //                         }
        //                     });
        //                 } else {
        //                     console.log(tracker);
        //                 }
        //             });
        //         }
        //     });
        // });

        socket.on('online', function (user) {
            console.log('#log -------> online');
            console.log(user);
            connectedUsers.push({ user_id: user.id, socket_id: socket.id })
            console.log(connectedUsers)
        });

        socket.on('disconnect', function () {
            console.log('#log -------> disconnect')
            connectedUsers.forEach((element,index) => {
                if (element.socket_id == socket.id) {
                    connectedUsers.splice(index, 1);
                }
            });
            console.log(connectedUsers)
        });
    });

    return io;
}

//export functions
module.exports = {
    listen,
    io,
    connectedUsers
}