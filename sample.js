let app = require('express')();
let http = require('http').Server(server);
let io = require('socket.io')(http);
const trackingService = require('./src/driver-model/busTracking/trackingService');
const journeyService = require('./src/driver-model/journeyData/journeyService');
const locationService = require('./src/driver-model/locationData/locationDataService');

io.on('connection', (socket) => {
    console.log('user connected');

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });

    socket.on('connect', function (busId) {
        console.log('user disconnected');
    });

    socket.on('location', (locationObj) => {
        var obj = {};
        obj.status = "Active";
        obj.bus_id = locationObj.bus.id;
        trackingService.checkActiveTracker(obj, "res", (data) => {
            //     console.log(data.status)
            //     console.log(data)
            if (locationObj.status == "Deactive") {
                journeyService.journeyEnd({ bus_id: locationObj.bus.id, tracker: data.data._id, endTime: locationObj.endTime }, "", (journey) => {
                    console.log(journey);
                    var location = {};
                    location.longitude = data.data.longitude;
                    location.latitude = data.data.latitude;
                    location.status = "Deactive";
                    location.tracking_id = data.data._id;
                    trackingService.trackingUpdate(location, "res", (updatedTracker) => {
                        if (updatedTracker.status) {
                            trackingService.getActiveTrackers(obj, "res", (trackers) => {
                                if (trackers.status) {
                                    io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                                } else {
                                    console.log(trackers.data);
                                }
                            });
                        } else {
                            console.log(updatedTracker.data);
                        }
                    });
                })
            }
            else if (data.status) {
                var location = {};
                location.longitude = locationObj.longitude;
                location.latitude = locationObj.latitude;
                location.status = "Active";
                location.tracking_id = data.data._id;
                trackingService.trackingUpdate(location, "res", (updatedTracker) => {
                    if (updatedTracker.status) {
                        trackingService.getActiveTrackers(obj, "res", (trackers) => {
                            if (trackers.status) {
                                io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                            } else {
                                console.log(trackers.data);
                            }
                        });
                    } else {
                        console.log(updatedTracker.data);
                    }
                });
            } else {
                var location = {};
                location.longitude = locationObj.longitude;
                location.latitude = locationObj.latitude;
                location.status = "Active";
                location.bus = locationObj.bus;

                trackingService.startTracking(location, " ", (tracker) => {
                    if (tracker.status) {
                        var journey = {};
                        journey.startTime = locationObj.startTime
                        journey.bus = locationObj.bus;
                        journey.tracker = tracker.data._id;
                        journeyService.startJourney(journey, "", (journeyStatus) => {
                            if (journeyStatus.status) {
                                trackingService.getActiveTrackers(obj, "res", (trackers) => {
                                    if (trackers.status) {
                                        io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                                    } else {
                                        console.log(trackers.data);
                                    }
                                });
                            } else {
                                console.log(journeyStatus);
                            }
                        });
                    } else {
                        console.log(tracker);
                    }
                });
            }
        });
    });

    socket.on('buses', (locationObj) => {
        var obj = {};
        obj.status = "Active";
        trackingService.getActiveTrackers(obj, "res", (trackers) => {
            if (trackers.status) {
                io.emit('trackers', { type: 'trackers', tracker: trackers.data });
            } else {
                console.log(trackers.data);
            }
        });
    });
});